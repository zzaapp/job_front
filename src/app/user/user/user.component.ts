import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Event, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private router: Router) {

  }

  ngOnInit() {
    if (this.router.url === '/user') {
      this.router.navigate(['dashboard'], {relativeTo: this.route});
    }
  }
}

