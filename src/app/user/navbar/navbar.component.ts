import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

// Services
import { UserService } from '@shared/services/user.service';

@Component({
    selector: 'app-user-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

    // Inputs
    @Input() currentPage: string;

    constructor(private userService: UserService) {}

}
