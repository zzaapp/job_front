import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute, Event, NavigationEnd } from '@angular/router';
import { isNullOrUndefined } from 'util';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { JobImageModel } from '@shared/models/job-image.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-edit-job-images',
  templateUrl: './edit-job-images.component.html',
  styleUrls: ['./edit-job-images.component.css']
})
export class EditJobImagesComponent implements OnInit {

  // Modals
  bsModalRef: BsModalRef;

  // Job Id
  private jobId: number;

  // Alert Message
  public alert: AlertMessage = new AlertMessage();

  // Variables
  public images: JobImageModel[];

  constructor(private activatedRoute: ActivatedRoute,
              private httpService: HttpService,
              private modalService: BsModalService) {}

  ngOnInit() {
    // Get id from URL
    this.jobId = +this.activatedRoute.snapshot.url[2].path;

    this.getJobImages(this.jobId);
  }

  private getJobImages(jobId: number) {
    this.httpService.getJobImages(jobId).subscribe(
      (data: JobImageModel[]) => {
        this.images = data;
      },
      (error: HttpErrorResponse) => {
        this.images = [];
      }
    );
  }

  public onFileChange(event) {
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
      this.uploadJobImage(file);
    }
  }

  public uploadJobImage(file) {
    this.alert.reset();

    this.httpService.uploadJobImage(this.jobId, file).subscribe(
        (data: JobImageModel) => {
            this.images.push(data);
            this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Job image uploaded.');
            this.showAlertModal(this.alert);
        },
        (error: HttpErrorResponse) => {
            const status    = error.status;
            switch (status) {
            case 400:
                const errorKey = HelperService.getObjectFirstKey(error.error);
                this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Upload image error.');
                break;
            case 403:
            case 404:
            case 500:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                break;
            default:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                break;
            }
            this.showAlertModal(this.alert);
        }
    );
  }

  public deleteImage(imageId: number) {
    this.alert.reset();

    this.httpService.deleteJobImage(this.jobId, imageId).subscribe(
        (data: JobImageModel) => {
            const indexToRemove = HelperService.findObjectIndexByCriteria(this.images, 'id', data.id, false);
            if(!isNullOrUndefined(indexToRemove)) {
              this.images.splice(indexToRemove, 1);
            }
            this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Job image deleted.');
            this.showAlertModal(this.alert);
        },
        (error: HttpErrorResponse) => {
            const status    = error.status;
            switch (status) {
            case 400:
                const errorKey = HelperService.getObjectFirstKey(error.error);
                this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Delete image error.');
                break;
            case 403:
            case 404:
            case 500:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                break;
            default:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                break;
            }
            this.showAlertModal(this.alert);
        }
    );
  }

  private showAlertModal(alert: AlertMessage) {
    const initialState = {
      alert: alert
    };

    switch (alert.type) {
      case AlertMessage.TYPE_ERROR:
        this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
        break;
      case AlertMessage.TYPE_SUCCESS:
        this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
        break;
      }

  }

}