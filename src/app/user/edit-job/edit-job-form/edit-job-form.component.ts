import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute, Event, NavigationEnd } from '@angular/router';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { JobModel } from '@shared/models/job.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-edit-job-form',
  templateUrl: './edit-job-form.component.html',
  styleUrls: ['./edit-job-form.component.css']
})
export class EditJobFormComponent implements OnInit {

  // Modals
  bsModalRef: BsModalRef;

  // Job Id
  private jobId: number;

  // Edit Job Form
  public editJobForm: FormGroup;
  public title:       FormControl;
  public category:    FormControl;
  public description: FormControl;
  public included:    FormControl;
  public duration:    FormControl;

  // Alert Message
  public alert: AlertMessage = new AlertMessage();

  constructor(private activatedRoute: ActivatedRoute,
              private httpService: HttpService,
              private modalService: BsModalService,) {}

  ngOnInit() {
    // Get id from URL
    this.jobId = +this.activatedRoute.snapshot.url[2].path;

    this.buildControls();
    this.buildForm();

    this.getUserJob(this.jobId);
  }

  private getUserJob(jobId: number) {
    this.httpService.getUserJobs(jobId).subscribe(
      (data: JobModel[]) => {
        this.populateForm(data[0]);
      },
      (error: HttpErrorResponse) => {
        this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
        this.showAlertModal(this.alert);
      }
    );
  }

  private buildControls() {
    this.title       = new FormControl('', [Validators.required]);
    this.category    = new FormControl('', [Validators.required]);
    this.description = new FormControl('', [Validators.required]);
    this.included    = new FormControl('', []);
    this.duration    = new FormControl('', []);
  }

  private buildForm() {
    this.editJobForm = new FormGroup({
        title:       this.title,
        category:    this.category,
        description: this.description,
        included:    this.included,
        duration:    this.duration
    });
  }

  private populateForm(job: JobModel) {
    this.title.setValue(job.title);
    this.category.setValue(job.category);
    this.description.setValue(job.description);
    this.included.setValue(job.included);
    this.duration.setValue(job.duration);
  }

  public canSubmit() {
    return this.editJobForm.valid;
  }

  public onCloseAlert() {
    this.alert.reset();
  }

  public onSubmitForm() {
    this.alert.reset();

    this.httpService.jobUpdate(this.jobId,
                               this.editJobForm.value.title,
                               this.editJobForm.value.category,
                               this.editJobForm.value.description,
                               this.editJobForm.value.included,
                               this.editJobForm.value.duration).subscribe(
        (data: JobModel) => {
            this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Your job has successfully been updated!');
            this.showAlertModal(this.alert);
        },
        (error: HttpErrorResponse) => {
            const status    = error.status;    
            switch (status) {
            case 400:
                const errorKey = HelperService.getObjectFirstKey(error.error);
                this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Update Job error.');
                break;
            case 403:
            case 404:
            case 500:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                break;
            default:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                break;
            }
            this.showAlertModal(this.alert);
        }
    );

  }

  private showAlertModal(alert: AlertMessage) {
    const initialState = {
      alert: alert
    };

    switch (alert.type) {
    case AlertMessage.TYPE_ERROR:
        this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
        break;
    case AlertMessage.TYPE_SUCCESS:
        this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
        break;
    }

  }

}