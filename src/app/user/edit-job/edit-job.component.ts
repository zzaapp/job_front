import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-user-edit-job',
  templateUrl: './edit-job.component.html',
  styleUrls: ['./edit-job.component.css']
})
export class EditJobComponent {

  // Breadcrumb Info
  public title: string = 'Edit a Job';
  public breadcrumbs: any[] = [
    {title: 'Home', route: '/'},
    {title: 'Edit Job', route: ''},
  ];

  constructor() {}

}