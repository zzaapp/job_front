import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { SocialModel } from '@shared/models/social.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-social-settings',
  templateUrl: './social-settings.component.html',
  styleUrls: ['./social-settings.component.css']
})
export class SocialSettingsComponent implements OnInit {

    // Modals
    bsModalRef: BsModalRef;

    // Social Form
    public socialForm: FormGroup;
    public facebook:   FormControl;
    public instagram:  FormControl;
    public twitter:    FormControl;
    public dribbble:   FormControl;

    // Alert Message
    public alert: AlertMessage = new AlertMessage();

    constructor(private httpService: HttpService,
                private modalService: BsModalService) {}

    ngOnInit() {
        this.buildControls();
        this.buildForm();

        this.getSocialAccounts();
    }

    private getSocialAccounts() {
        this.httpService.getSocialAccounts().subscribe(
            (data: SocialModel) => {
                this.populateForm(data);
            },
            (error: HttpErrorResponse) => {}
        );
    }

    private buildControls() {
        this.facebook  = new FormControl('', []);
        this.instagram = new FormControl('', []);
        this.twitter   = new FormControl('', []);
        this.dribbble  = new FormControl('', []);
    }

    private buildForm() {
        this.socialForm = new FormGroup({
            facebook:  this.facebook,
            instagram: this.instagram,
            twitter:   this.twitter,
            dribbble:  this.dribbble
        });
    }

    private populateForm(socialAcc: SocialModel) {
        this.facebook.setValue(socialAcc.facebook);
        this.instagram.setValue(socialAcc.instagram);
        this.twitter.setValue(socialAcc.twitter);
        this.dribbble.setValue(socialAcc.dribbble);
    }

    public canSubmit() {
        return this.socialForm.valid;
    }

    public onCloseAlert() {
        this.alert.reset();
    }

    public onSubmitForm() {
        this.alert.reset();

        this.httpService.updateSocialAccounts(this.socialForm.value.facebook,
                                              this.socialForm.value.instagram,
                                              this.socialForm.value.twitter,
                                              this.socialForm.value.dribbble).subscribe(
            (data: any) => {
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Social accounts settings saved.');
                this.showAlertModal(this.alert);
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;    
                switch (status) {
                case 400:
                    const errorKey = HelperService.getObjectFirstKey(error.error);
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Social accounts settings error.');
                    break;
                case 403:
                case 404:
                case 500:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
                this.showAlertModal(this.alert);
            }
        );

    }

    private showAlertModal(alert: AlertMessage) {
        const initialState = {
            alert: alert
        };

        switch (alert.type) {
        case AlertMessage.TYPE_ERROR:
            this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
            break;
        case AlertMessage.TYPE_SUCCESS:
            this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
            break;
        }

    }

}