import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';
import { UserService } from '@shared/services/user.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { UserInfoModel } from '@shared/models/user-info.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-info-settings',
  templateUrl: './info-settings.component.html',
  styleUrls: ['./info-settings.component.css']
})
export class InfoSettingsComponent implements OnInit {

    // Modals
    bsModalRef: BsModalRef;

    // Social Form
    public infoForm:        FormGroup;
    public firstName:       FormControl;
    public lastName:        FormControl;
    public country:         FormControl;
    public county:          FormControl;
    public address:         FormControl;
    public spokenLanguages: FormControl;
    public hobbies:         FormControl;
    public curiosities:     FormControl;

    // Alert Message
    public alert: AlertMessage = new AlertMessage();

    constructor(private httpService: HttpService,
                private modalService: BsModalService,
                private userService: UserService) {}

    ngOnInit() {
        this.buildControls();
        this.buildForm();

        this.getUserInfo();
    }

    private getUserInfo() {
        this.httpService.getUserInfo().subscribe(
            (data: UserInfoModel) => {
                console.log(data);
                this.populateForm(data);
            },
            (error: HttpErrorResponse) => {}
        );
    }

    private buildControls() {
        this.firstName       = new FormControl('', [Validators.required]);
        this.lastName        = new FormControl('', [Validators.required]);
        this.country         = new FormControl('', []);
        this.county          = new FormControl('', []);
        this.address         = new FormControl('', []);
        this.spokenLanguages = new FormControl('', []);
        this.hobbies         = new FormControl('', []);
        this.curiosities     = new FormControl('', []);
    }

    private buildForm() {
        this.infoForm = new FormGroup({
            firstName:       this.firstName,
            lastName:        this.lastName,
            country:         this.country,
            county:          this.county,
            address:         this.address,
            spokenLanguages: this.spokenLanguages,
            hobbies:         this.hobbies,
            curiosities:     this.curiosities,
        });
    }

    private populateForm(userInfo: UserInfoModel) {
        this.firstName.setValue(userInfo.first_name);
        this.lastName.setValue(userInfo.last_name);
        this.country.setValue(userInfo.country);
        this.county.setValue(userInfo.county);
        this.address.setValue(userInfo.address);
        this.spokenLanguages.setValue(userInfo.spoken_languages);
        this.hobbies.setValue(userInfo.hobbies);
        this.curiosities.setValue(userInfo.curiosities);
    }

    public canSubmit() {
        return this.infoForm.valid;
    }

    public onCloseAlert() {
        this.alert.reset();
    }

    public onSubmitForm() {
        this.alert.reset();

        this.httpService.updateUserInfo(this.infoForm.value.firstName,
                                        this.infoForm.value.lastName,
                                        this.infoForm.value.country,
                                        this.infoForm.value.county,
                                        this.infoForm.value.address,
                                        this.infoForm.value.spokenLanguages,
                                        this.infoForm.value.hobbies,
                                        this.infoForm.value.curiosities).subscribe(
            (data: UserInfoModel) => {
                this.userService.updateUserName(data.first_name, data.last_name);
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'User informations saved.');
                this.showAlertModal(this.alert);
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;    
                switch (status) {
                case 400:
                    const errorKey = HelperService.getObjectFirstKey(error.error);
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'User informations error.');
                    break;
                case 403:
                case 404:
                case 500:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
                this.showAlertModal(this.alert);
            }
        );

    }

    private showAlertModal(alert: AlertMessage) {
        const initialState = {
            alert: alert
        };

        switch (alert.type) {
        case AlertMessage.TYPE_ERROR:
            this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
            break;
        case AlertMessage.TYPE_SUCCESS:
            this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
            break;
        }

    }

}