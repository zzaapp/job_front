import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { isNullOrUndefined } from 'util';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';
import { NotifyService } from '@shared/services/notify.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { CvJobModel } from '@shared/models/cv-job.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-cv-job-list',
  templateUrl: './cv-jobs-list.component.html',
  styleUrls: ['./cv-jobs-list.component.css']
})
export class CvJobsListComponent implements OnInit, OnDestroy {

  // Modals
  bsModalRef: BsModalRef;

  // Alert Message
  public alert: AlertMessage = new AlertMessage();

  // Variables
  public jobs: CvJobModel[];

  // Subscriptions
  private addCvJobSubscription: Subscription;

  constructor(private httpService: HttpService,
              private modalService: BsModalService,
              private notifyService: NotifyService) {}

  ngOnInit() {
    this.getCvJobs();
    this.initSubscriptions();
  }

  ngOnDestroy() {
    this.addCvJobSubscription.unsubscribe();
  }

  private getCvJobs() {
    this.httpService.cvJobsGet().subscribe(
      (data: CvJobModel[]) => {
        this.jobs = data;
      },
      (error: HttpErrorResponse) => {
        this.jobs = [];
      }
    );
  }

  private initSubscriptions() {
    this.addCvJobSubscription = this.notifyService.newCvJob.subscribe((data: CvJobModel) => {
        this.jobs.push(data);
    });
  }

  public deleteCvJob(jobId: number) {
    this.alert.reset();

    this.httpService.cvJobDelete(jobId).subscribe(
        (data: CvJobModel) => {
            const indexToRemove = HelperService.findObjectIndexByCriteria(this.jobs, 'id', data.id, false);
            if(!isNullOrUndefined(indexToRemove)) {
              this.jobs.splice(indexToRemove, 1);
            }
            this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Cv job deleted.');
            this.showAlertModal(this.alert);
        },
        (error: HttpErrorResponse) => {
            const status    = error.status;
            switch (status) {
            case 400:
                const errorKey = HelperService.getObjectFirstKey(error.error);
                this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Delete cv job error.');
                break;
            case 403:
            case 404:
            case 500:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                break;
            default:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                break;
            }
            this.showAlertModal(this.alert);
        }
    );
  }

  private showAlertModal(alert: AlertMessage) {
    const initialState = {
      alert: alert
    };

    switch (alert.type) {
      case AlertMessage.TYPE_ERROR:
        this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
        break;
      case AlertMessage.TYPE_SUCCESS:
        this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
        break;
      }

  }

}