import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';
import { UserService } from '@shared/services/user.service';
import { NotifyService } from '@shared/services/notify.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { CvJobModel } from '@shared/models/cv-job.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-cv-jobs-form',
  templateUrl: './cv-jobs-form.component.html',
  styleUrls: ['./cv-jobs-form.component.css']
})
export class CvJobsFormComponent implements OnInit {

    // Modals
    bsModalRef: BsModalRef;

    // Cv Jobs Form
    public cvJobsForm:       FormGroup;
    public position:         FormControl;
    public company:          FormControl;
    public yearStart:        FormControl;
    public yearEnd:          FormControl;
    public responsibilities: FormControl;

    // Alert Message
    public alert: AlertMessage = new AlertMessage();

    constructor(private httpService: HttpService,
                private modalService: BsModalService,
                private notifyService: NotifyService) {}

    ngOnInit() {
        this.buildControls();
        this.buildForm();
    }

    private buildControls() {
        this.position         = new FormControl('', [Validators.required]);
        this.company          = new FormControl('', [Validators.required]);
        this.yearStart        = new FormControl('', [Validators.required]);
        this.yearEnd          = new FormControl('', [Validators.required]);
        this.responsibilities = new FormControl('', [Validators.required]);
    }

    private buildForm() {
        this.cvJobsForm = new FormGroup({
            position:         this.position,
            company:          this.company,
            yearStart:        this.yearStart,
            yearEnd:          this.yearEnd,
            responsibilities: this.responsibilities
        });
    }

    public canSubmit() {
        return this.cvJobsForm.valid;
    }

    public onCloseAlert() {
        this.alert.reset();
    }

    public onSubmitForm() {
        this.alert.reset();

        this.httpService.cvJobCreate(this.cvJobsForm.value.position,
                                     this.cvJobsForm.value.company,
                                     this.cvJobsForm.value.yearStart,
                                     this.cvJobsForm.value.yearEnd,
                                     this.cvJobsForm.value.responsibilities).subscribe(
            (data: CvJobModel) => {
                this.resetForm();
                this.notifyService.newCvJob.emit(data);
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Cv job saved.');
                this.showAlertModal(this.alert);
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;    
                switch (status) {
                case 400:
                    const errorKey = HelperService.getObjectFirstKey(error.error);
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Cv job error.');
                    break;
                case 403:
                case 404:
                case 500:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
                this.showAlertModal(this.alert);
            }
        );

    }

    private showAlertModal(alert: AlertMessage) {
        const initialState = {
            alert: alert
        };

        switch (alert.type) {
        case AlertMessage.TYPE_ERROR:
            this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
            break;
        case AlertMessage.TYPE_SUCCESS:
            this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
            break;
        }

    }

    private resetForm() {
        this.cvJobsForm.reset();
    }

}