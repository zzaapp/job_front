import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';
import { NotifyService } from '@shared/services/notify.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { CvEducationModel } from '@shared/models/cv-education.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-cv-education-form',
  templateUrl: './cv-education-form.component.html',
  styleUrls: ['./cv-education-form.component.css']
})
export class CvEducationFormComponent implements OnInit {

    // Modals
    bsModalRef: BsModalRef;

    // Cv Education Form
    public cvEducationForm: FormGroup;
    public type:            FormControl;
    public institution:     FormControl;
    public specialization:  FormControl;
    public city:            FormControl;
    public yearStart:       FormControl;
    public yearEnd:         FormControl;

    // Alert Message
    public alert: AlertMessage = new AlertMessage();

    constructor(private httpService: HttpService,
                private modalService: BsModalService,
                private notifyService: NotifyService) {}

    ngOnInit() {
        this.buildControls();
        this.buildForm();
    }

    private buildControls() {
        this.type           = new FormControl('Elementary', [Validators.required]);
        this.institution    = new FormControl('', [Validators.required]);
        this.specialization = new FormControl('', [Validators.required]);
        this.city           = new FormControl('', [Validators.required]);
        this.yearStart      = new FormControl('', [Validators.required]);
        this.yearEnd        = new FormControl('', [Validators.required]);
    }

    private buildForm() {
        this.cvEducationForm = new FormGroup({
            type:           this.type,
            institution:    this.institution,
            specialization: this.specialization,
            city:           this.city,
            yearStart:      this.yearStart,
            yearEnd:        this.yearEnd
        });
    }

    public canSubmit() {
        return this.cvEducationForm.valid;
    }

    public onCloseAlert() {
        this.alert.reset();
    }

    public onSubmitForm() {
        this.alert.reset();

        this.httpService.cvEducationCreate(this.cvEducationForm.value.type,
                                           this.cvEducationForm.value.institution,
                                           this.cvEducationForm.value.specialization,
                                           this.cvEducationForm.value.city,
                                           this.cvEducationForm.value.yearStart,
                                           this.cvEducationForm.value.yearEnd).subscribe(
            (data: CvEducationModel) => {
                this.resetForm();
                this.notifyService.newCvEducation.emit(data);
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Cv education saved.');
                this.showAlertModal(this.alert);
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;    
                switch (status) {
                case 400:
                    const errorKey = HelperService.getObjectFirstKey(error.error);
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Cv education error.');
                    break;
                case 403:
                case 404:
                case 500:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
                this.showAlertModal(this.alert);
            }
        );

    }

    private showAlertModal(alert: AlertMessage) {
        const initialState = {
            alert: alert
        };

        switch (alert.type) {
        case AlertMessage.TYPE_ERROR:
            this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
            break;
        case AlertMessage.TYPE_SUCCESS:
            this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
            break;
        }

    }

    private resetForm() {
        this.cvEducationForm.reset();
        this.cvEducationForm.controls['type'].setValue('Elementary');
    }

}