import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { isNullOrUndefined } from 'util';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';
import { NotifyService } from '@shared/services/notify.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { CvEducationModel } from '@shared/models/cv-education.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-cv-education-list',
  templateUrl: './cv-education-list.component.html',
  styleUrls: ['./cv-education-list.component.css']
})
export class CvEducationListComponent implements OnInit, OnDestroy {

  // Modals
  bsModalRef: BsModalRef;

  // Alert Message
  public alert: AlertMessage = new AlertMessage();

  // Variables
  public educations: CvEducationModel[];

  // Subscriptions
  private addCvEducationSubscription: Subscription;

  constructor(private httpService: HttpService,
              private modalService: BsModalService,
              private notifyService: NotifyService) {}

  ngOnInit() {
    this.getCvEducation();
    this.initSubscriptions();
  }

  ngOnDestroy() {
    this.addCvEducationSubscription.unsubscribe();
  }

  private getCvEducation() {
    this.httpService.cvEducationGet().subscribe(
      (data: CvEducationModel[]) => {
        this.educations = data;
      },
      (error: HttpErrorResponse) => {
        this.educations = [];
      }
    );
  }

  private initSubscriptions() {
    this.addCvEducationSubscription = this.notifyService.newCvEducation.subscribe((data: CvEducationModel) => {
        this.educations.push(data);
    });
  }

  public deleteCvEducation(educationId: number) {
    this.alert.reset();

    this.httpService.cvEducationDelete(educationId).subscribe(
        (data: CvEducationModel) => {
            const indexToRemove = HelperService.findObjectIndexByCriteria(this.educations, 'id', data.id, false);
            if(!isNullOrUndefined(indexToRemove)) {
              this.educations.splice(indexToRemove, 1);
            }
            this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Cv education deleted.');
            this.showAlertModal(this.alert);
        },
        (error: HttpErrorResponse) => {
            const status    = error.status;
            switch (status) {
            case 400:
                const errorKey = HelperService.getObjectFirstKey(error.error);
                this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Delete education error.');
                break;
            case 403:
            case 404:
            case 500:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                break;
            default:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                break;
            }
            this.showAlertModal(this.alert);
        }
    );
  }

  private showAlertModal(alert: AlertMessage) {
    const initialState = {
      alert: alert
    };

    switch (alert.type) {
      case AlertMessage.TYPE_ERROR:
        this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
        break;
      case AlertMessage.TYPE_SUCCESS:
        this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
        break;
      }

  }

}