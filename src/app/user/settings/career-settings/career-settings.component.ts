import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { CareerInfoModel } from '@shared/models/career-info.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-career-settings',
  templateUrl: './career-settings.component.html',
  styleUrls: ['./career-settings.component.css']
})
export class CareerSettingsComponent implements OnInit {

    // Modals
    bsModalRef: BsModalRef;

    // Career Form
    public careerForm: FormGroup;
    public level:      FormControl;
    public category:   FormControl;

    // Alert Message
    public alert: AlertMessage = new AlertMessage();

    constructor(private httpService: HttpService,
                private modalService: BsModalService) {}

    ngOnInit() {
        this.buildControls();
        this.buildForm();

        this.getCareerInfo();
    }

    private getCareerInfo() {
        this.httpService.getUserCareerInfo().subscribe(
            (data: CareerInfoModel) => {
                console.log(data);
                const form = new CareerInfoModel(data);
                this.populateForm(form);
            },
            (error: HttpErrorResponse) => {}
        );
    }

    private buildControls() {
        this.level    = new FormControl('', [Validators.required]);
        this.category = new FormControl('', [Validators.required]);
    }

    private buildForm() {
        this.careerForm = new FormGroup({
            level:    this.level,
            category: this.category
        });
    }

    private populateForm(careerInfo: CareerInfoModel) {
        this.level.setValue(careerInfo.level);
        this.category.setValue(careerInfo.category);
    }

    public canSubmit() {
        return this.careerForm.valid;
    }

    public onCloseAlert() {
        this.alert.reset();
    }

    public onSubmitForm() {
        this.alert.reset();

        this.httpService.updateUserCareerInfo(this.careerForm.value.level,
                                              this.careerForm.value.category).subscribe(
            (data: CareerInfoModel) => {
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'User career informations saved.');
                this.showAlertModal(this.alert);
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;    
                switch (status) {
                case 400:
                    const errorKey = HelperService.getObjectFirstKey(error.error);
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'User career informations error.');
                    break;
                case 403:
                case 404:
                case 500:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
                this.showAlertModal(this.alert);
            }
        );

    }

    private showAlertModal(alert: AlertMessage) {
        const initialState = {
            alert: alert
        };

        switch (alert.type) {
        case AlertMessage.TYPE_ERROR:
            this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
            break;
        case AlertMessage.TYPE_SUCCESS:
            this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
            break;
        }

    }

}