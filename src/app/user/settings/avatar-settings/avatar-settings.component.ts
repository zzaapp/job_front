import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';
import { UserService } from '@shared/services/user.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { UserInfoModel } from '@shared/models/user-info.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-avatar-settings',
  templateUrl: './avatar-settings.component.html',
  styleUrls: ['./avatar-settings.component.css']
})
export class AvatarSettingsComponent implements OnInit {

    // Variables
    public imgUrl: string = null;

    // Modals
    bsModalRef: BsModalRef;

    // Alert Message
    public alert: AlertMessage = new AlertMessage();

    constructor(private httpService: HttpService,
                private modalService: BsModalService,
                private userService: UserService) {}

    ngOnInit() {
        this.initImageValue();
    }

    private initImageValue() {
        this.imgUrl = (this.userService.getUser() && this.userService.getUser().thumbnail)?
                      this.userService.getUser().thumbnail : 'assets/img/authplc.png';
    }

    public onFileChange(event) {
        if(event.target.files.length > 0) {
          let file = event.target.files[0];
          this.uploadAvatar(file);
        }
    }

    public onCloseAlert() {
        this.alert.reset();
    }

    public deleteUserAvatar() {
        this.alert.reset();

        this.httpService.deleteUserAvatar().subscribe(
            (data: any) => {
                this.imgUrl = 'assets/img/authplc.png';
                this.userService.changeThumbnail(null);
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'User avatar deleted.');
                this.showAlertModal(this.alert);
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;
                switch (status) {
                case 400:
                    const errorKey = HelperService.getObjectFirstKey(error.error);
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Delete avatar error.');
                    break;
                case 403:
                case 404:
                case 500:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
                this.showAlertModal(this.alert);
            }
        );
    }

    public uploadAvatar(file) {
        this.alert.reset();

        this.httpService.uploadUserAvatar(file).subscribe(
            (data: UserInfoModel) => {
                this.imgUrl = data.thumbnail;
                this.userService.changeThumbnail(data.thumbnail);
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'User avatar uploaded.');
                this.showAlertModal(this.alert);
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;
                switch (status) {
                case 400:
                    const errorKey = HelperService.getObjectFirstKey(error.error);
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Upload avatar error.');
                    break;
                case 403:
                case 404:
                case 500:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
                this.showAlertModal(this.alert);
            }
        );
    }

    private showAlertModal(alert: AlertMessage) {
        const initialState = {
            alert: alert
        };

        switch (alert.type) {
        case AlertMessage.TYPE_ERROR:
            this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
            break;
        case AlertMessage.TYPE_SUCCESS:
            this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
            break;
        }

    }

}