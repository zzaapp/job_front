import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-user-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent {

  // Breadcrumb Info
  public title: string = 'Settings';
  public breadcrumbs: any[] = [
    {title: 'Home', route: '/'},
    {title: 'Settings', route: '/user/settings'},
  ];

}