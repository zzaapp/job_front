import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserRoutingModule} from './user-routing.module';
import {SharedModule} from '@shared/shared.module';
import {ModalModule, TooltipModule, TypeaheadModule} from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

// Components
import { UserComponent } from './user/user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ApplicationRowComponent } from './dashboard/application-row/application-row.component';
import { SettingsComponent } from './settings/settings.component';
import { SocialSettingsComponent } from './settings/social-settings/social-settings.component';
import { InfoSettingsComponent } from './settings/info-settings/info-settings.component';
import { AvatarSettingsComponent } from './settings/avatar-settings/avatar-settings.component';
import { AddJobComponent } from './add-job/add-job.component';
import { BecomeProviderComponent } from './add-job/become-provider/become-provider.component';
import { AddJobFormComponent } from './add-job/add-job-form/add-job-form.component';
import { JobsComponent } from './jobs/jobs.component';
import { JobBoxComponent } from './jobs/job-box/job-box.component';
import { NavbarComponent } from './navbar/navbar.component';
import { EditJobComponent } from './edit-job/edit-job.component';
import { EditJobFormComponent } from './edit-job/edit-job-form/edit-job-form.component';
import { EditJobImagesComponent } from './edit-job/edit-job-images/edit-job-images.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { AppointmentRowComponent } from './appointments/appointment-row/appointment-row.component';
import { CompanyComponent } from './company/company.component';
import { KeyPersonsComponent } from './key-persons/key-persons.component';
import { KeyPersonsFormComponent } from './key-persons/key-persons-form/key-persons-form.component';
import { KeyPersonsListComponent } from './key-persons/key-persons-list/key-persons-list.component';
import { CvJobsSettingsComponent } from './settings/cv-jobs-settings/cv-jobs-settings.component';
import { CvJobsFormComponent } from './settings/cv-jobs-settings/cv-jobs-form/cv-jobs-form.component';
import { CvJobsListComponent } from './settings/cv-jobs-settings/cv-jobs-list/cv-jobs-list.component';
import { CvEducationSettingsComponent } from './settings/cv-education-settings/cv-education-settings.component';
import { CvEducationFormComponent } from './settings/cv-education-settings/cv-education-form/cv-education-form.component';
import { CvEducationListComponent } from './settings/cv-education-settings/cv-education-list/cv-education-list.component';
import { CareerSettingsComponent } from './settings/career-settings/career-settings.component';

// Guards
import { UserGuard } from '@shared/guards/user.guard';

@NgModule({
    imports: [
        CommonModule,
        UserRoutingModule,
        SharedModule,
        ModalModule.forRoot(),
        TooltipModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TypeaheadModule.forRoot()
    ],
    exports: [],
    declarations: [
        UserComponent,
        DashboardComponent,
        ApplicationRowComponent,
        SettingsComponent,
        SocialSettingsComponent,
        InfoSettingsComponent,
        AvatarSettingsComponent,
        AddJobComponent,
        BecomeProviderComponent,
        AddJobFormComponent,
        JobsComponent,
        JobBoxComponent,
        NavbarComponent,
        EditJobComponent,
        EditJobFormComponent,
        EditJobImagesComponent,
        AppointmentsComponent,
        AppointmentRowComponent,
        CompanyComponent,
        KeyPersonsComponent,
        KeyPersonsFormComponent,
        KeyPersonsListComponent,
        CvJobsSettingsComponent,
        CvJobsFormComponent,
        CvJobsListComponent,
        CvEducationSettingsComponent,
        CvEducationFormComponent,
        CvEducationListComponent,
        CareerSettingsComponent
    ],
    providers: [
        UserGuard
    ],
    entryComponents: [
    ]
})
export class UserModule {
}
