import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';
import { UserService } from '@shared/services/user.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { CompanyModel } from '@shared/models/company.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-become-provider',
  templateUrl: './become-provider.component.html',
  styleUrls: ['./become-provider.component.css']
})
export class BecomeProviderComponent implements OnInit {

  // Modals
  bsModalRef: BsModalRef;

  // Provider Form
  public providerForm:       FormGroup;
  public phone:              FormControl;
  public companyName:        FormControl;
  public companyDescription: FormControl;
  public companyEvolution:   FormControl;
  public companyCurrent:     FormControl;

  // Alert Message
  public alert: AlertMessage = new AlertMessage();

  constructor(private httpService: HttpService,
              private modalService: BsModalService,
              private userService: UserService) {}

  ngOnInit() {
    this.buildControls();
    this.buildForm();
  }

  private buildControls() {
    this.phone              = new FormControl('', [Validators.required]);
    this.companyName        = new FormControl('', [Validators.required]);
    this.companyDescription = new FormControl('', [Validators.required]);
    this.companyEvolution   = new FormControl('', []);
    this.companyCurrent     = new FormControl('', []);
  }

  private buildForm() {
    this.providerForm = new FormGroup({
      phone:              this.phone,
      companyName:        this.companyName,
      companyDescription: this.companyDescription,
      companyEvolution:   this.companyEvolution,
      companyCurrent:     this.companyCurrent
    });
  }

  public canSubmit() {
    return this.providerForm.valid;
  }

  public onCloseAlert() {
    this.alert.reset();
  }

  public onSubmitForm() {
    this.alert.reset();

    this.httpService.providerCreate(this.providerForm.value.phone,
                                    this.providerForm.value.companyName,
                                    this.providerForm.value.companyDescription,
                                    this.providerForm.value.companyEvolution,
                                    this.providerForm.value.companyCurrent).subscribe(
        (data: CompanyModel) => {
            this.userService.becomeProvider();
            this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'You are now a provider!');
            this.showAlertModal(this.alert);
        },
        (error: HttpErrorResponse) => {
            const status    = error.status;    
            switch (status) {
            case 400:
                const errorKey = HelperService.getObjectFirstKey(error.error);
                this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Provider error.');
                break;
            case 403:
            case 404:
            case 500:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                break;
            default:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                break;
            }
            this.showAlertModal(this.alert);
        }
    );

  }

  private showAlertModal(alert: AlertMessage) {
    const initialState = {
      alert: alert
    };

    switch (alert.type) {
    case AlertMessage.TYPE_ERROR:
        this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
        break;
    case AlertMessage.TYPE_SUCCESS:
        this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
        break;
    }

  }

}