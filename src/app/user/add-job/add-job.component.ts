import { Component, OnInit, OnDestroy } from '@angular/core';

// Services
import { UserService } from '@shared/services/user.service';

@Component({
  selector: 'app-user-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.css']
})
export class AddJobComponent {

  // Breadcrumb Info
  public title: string = 'Add a Job';
  public breadcrumbs: any[] = [
    {title: 'Home', route: '/'},
    {title: 'Add A Job', route: '/user/job/create'},
  ];

  constructor(private userService: UserService) {}

}