import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-add-job-form',
  templateUrl: './add-job-form.component.html',
  styleUrls: ['./add-job-form.component.css']
})
export class AddJobFormComponent implements OnInit {

  // Modals
  bsModalRef: BsModalRef;

  // Add Job Form
  public addJobForm:  FormGroup;
  public title:       FormControl;
  public category:    FormControl;
  public description: FormControl;
  public included:    FormControl;
  public duration:    FormControl;

  // Alert Message
  public alert: AlertMessage = new AlertMessage();

  constructor(private httpService: HttpService,
              private modalService: BsModalService) {}

  ngOnInit() {
    this.buildControls();
    this.buildForm();
  }

  private buildControls() {
    this.title       = new FormControl('', [Validators.required]);
    this.category    = new FormControl('it', [Validators.required]);
    this.description = new FormControl('', [Validators.required]);
    this.included    = new FormControl('', []);
    this.duration    = new FormControl('', []);
  }

  private buildForm() {
    this.addJobForm = new FormGroup({
        title:       this.title,
        category:    this.category,
        description: this.description,
        included:    this.included,
        duration:    this.duration
    });
  }

  public canSubmit() {
    return this.addJobForm.valid;
  }

  public onCloseAlert() {
    this.alert.reset();
  }

  public onSubmitForm() {
    this.alert.reset();

    this.httpService.jobCreate(this.addJobForm.value.title,
                               this.addJobForm.value.category,
                               this.addJobForm.value.description,
                               this.addJobForm.value.included,
                               this.addJobForm.value.duration).subscribe(
        (data: any) => {
            this.resetForm();
            this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Your job is now posted!');
            this.showAlertModal(this.alert);
        },
        (error: HttpErrorResponse) => {
            const status    = error.status;    
            switch (status) {
            case 400:
                const errorKey = HelperService.getObjectFirstKey(error.error);
                this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Create Job error.');
                break;
            case 403:
            case 404:
            case 500:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                break;
            default:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                break;
            }
            this.showAlertModal(this.alert);
        }
    );

  }

  private showAlertModal(alert: AlertMessage) {
    const initialState = {
      alert: alert
    };

    switch (alert.type) {
    case AlertMessage.TYPE_ERROR:
        this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
        break;
    case AlertMessage.TYPE_SUCCESS:
        this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
        break;
    }

  }

  private resetForm() {
    this.addJobForm.reset();
    this.addJobForm.controls['category'].setValue('it');
  }

}