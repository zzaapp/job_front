import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AppointmentModel } from '@shared/models/appointment.model';

@Component({
  selector: 'app-user-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.css']
})
export class AppointmentsComponent {

  // Breadcrumb Info
  public title: string = 'Dashboard';
  public breadcrumbs: any[] = [
    {title: 'Home', route: '/'},
    {title: 'Appointments', route: '/user/appointments'},
  ];

  // Variables
  public appointments: AppointmentModel[];

  constructor(private httpService: HttpService) {}

  ngOnInit() {
    this.getUserApplications();
  }

  public getUserApplications() {

    this.httpService.getUserAppointments().subscribe(
        (data: AppointmentModel[]) => {

          this.appointments = [];
          data.forEach(element => {
              this.appointments.push(new AppointmentModel(element));
          });

        },
        (error: HttpErrorResponse) => {
          this.appointments = [];
        }
    );

  }

}