import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { ApplicationModel } from '@shared/models/application.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-appointment-row',
  templateUrl: './appointment-row.component.html',
  styleUrls: ['./appointment-row.component.css']
})
export class AppointmentRowComponent implements OnInit {

    // Inputs
    @Input() appointment: ApplicationModel;

    // Modals
    bsModalRef: BsModalRef;

    // Alert Message
    public alert: AlertMessage = new AlertMessage();

    constructor(private httpService: HttpService,
                private modalService: BsModalService) {}

    ngOnInit() {}

    public accept(appointmentId: number) {
        this.alert.reset();
    
        this.httpService.appointmentAccept(+appointmentId).subscribe(
            (data: ApplicationModel) => {
                this.appointment.status = data.status;
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Appointment has successfully been accepted!');
                this.showAlertModal(this.alert);
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;    
                switch (status) {
                case 400:
                    const errorKey = HelperService.getObjectFirstKey(error.error);
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Accept appointment error.');
                    break;
                case 403:
                case 404:
                case 500:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
                this.showAlertModal(this.alert);
            }
        );
    }

    public decline(appointmentId: number) {
        this.alert.reset();
    
        this.httpService.appointmentDecline(+appointmentId).subscribe(
            (data: ApplicationModel) => {
                this.appointment.status = data.status;
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Appointment has successfully been declined!');
                this.showAlertModal(this.alert);
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;    
                switch (status) {
                case 400:
                    const errorKey = HelperService.getObjectFirstKey(error.error);
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Decline appointment error.');
                    break;
                case 403:
                case 404:
                case 500:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
                this.showAlertModal(this.alert);
            }
        );
    }
    
    private showAlertModal(alert: AlertMessage) {
        const initialState = {
        alert: alert
        };
    
        switch (alert.type) {
        case AlertMessage.TYPE_ERROR:
            this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
            break;
        case AlertMessage.TYPE_SUCCESS:
            this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
            break;
        }
    }
}