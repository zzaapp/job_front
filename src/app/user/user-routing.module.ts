import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { UserComponent } from './user/user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { JobsComponent } from './jobs/jobs.component';
import { AddJobComponent } from './add-job/add-job.component';
import { EditJobComponent } from './edit-job/edit-job.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { CompanyComponent } from './company/company.component';
import { KeyPersonsComponent } from './key-persons/key-persons.component';

// Guards
import { UserGuard } from '@shared/guards/user.guard';

export const userRoutes: Routes = [
    { path: 'user',  component: UserComponent, canActivate: [UserGuard], children: [
        { path: 'dashboard', component: DashboardComponent },
        { path: 'settings', component: SettingsComponent },
        { path: 'jobs', component: JobsComponent },
        { path: 'job/create', component: AddJobComponent },
        { path: 'job/edit/:id', component: EditJobComponent },
        { path: 'appointments', component: AppointmentsComponent },
        { path: 'company', component: CompanyComponent },
        { path: 'key-persons', component: KeyPersonsComponent }
    ]}
];
@NgModule({
    imports: [
        RouterModule.forChild(userRoutes),
    ],
    exports: [
        RouterModule
    ]
})
export class UserRoutingModule { }
