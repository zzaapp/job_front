import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { JobModel } from '@shared/models/job.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-job-box',
  templateUrl: './job-box.component.html',
  styleUrls: ['./job-box.component.css']
})
export class JobBoxComponent implements OnInit {

    // Inputs
    @Input() job: JobModel;

    // Outputs
    @Output() jobDeleted = new EventEmitter<number>();

    // Modals
    bsModalRef: BsModalRef;

    // Alert Message
    public alert: AlertMessage = new AlertMessage();

    constructor(private httpService: HttpService,
                private modalService: BsModalService) {}

    ngOnInit() {}

    public changeStatus(active: boolean) {
        this.alert.reset();

        this.httpService.jobChangeStatus(this.job.id,
                                         active).subscribe(
            (data: JobModel) => {
                this.job.active = data.active;
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Job status successfully changed!');
                this.showAlertModal(this.alert);
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;    
                switch (status) {
                case 400:
                    const errorKey = HelperService.getObjectFirstKey(error.error);
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'We cannot change status of this job.');
                    break;
                case 403:
                case 404:
                case 500:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
                this.showAlertModal(this.alert);
            }
        );
    }

    public deleteJob() {
        this.alert.reset();

        this.httpService.jobDelete(this.job.id).subscribe(
            (data: any) => {
                this.jobDeleted.emit(this.job.id);
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Job successfully deleted!');
                this.showAlertModal(this.alert);
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;    
                switch (status) {
                case 400:
                    const errorKey = HelperService.getObjectFirstKey(error.error);
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'We cannot delete this job.');
                    break;
                case 403:
                case 404:
                case 500:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
                this.showAlertModal(this.alert);
            }
        );
    }

    private showAlertModal(alert: AlertMessage) {
        const initialState = {
          alert: alert
        };
    
        switch (alert.type) {
        case AlertMessage.TYPE_ERROR:
            this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
            break;
        case AlertMessage.TYPE_SUCCESS:
            this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
            break;
        }
    
    }
}