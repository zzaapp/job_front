import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { isNullOrUndefined } from 'util';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { JobModel } from '@shared/models/job.model';

@Component({
  selector: 'app-user-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {

  // Breadcrumb Info
  public title: string = 'Manage Jobs';
  public breadcrumbs: any[] = [
    {title: 'Home', route: '/'},
    {title: 'Manage Jobs', route: '/user/jobs'},
  ];

  // Alert Message
  public alert: AlertMessage = new AlertMessage();

  // Jobs
  public userJobs: JobModel[] = [];

  constructor(private httpService: HttpService) {}

  ngOnInit() {
    this.getUserJobs();
  }

  private getUserJobs() {
    this.httpService.getUserJobs().subscribe(
        (data: JobModel[]) => {
            this.userJobs = data;
        },
        (error: HttpErrorResponse) => {
            this.userJobs = [];
        }
    );
  }

  public onDeleteJob(jobId: number) {
    const index = HelperService.findObjectIndexByCriteria(this.userJobs, 'id', jobId, false);
    if(!isNullOrUndefined(index)) {
      this.userJobs.splice(index, 1);
    }
  }

}