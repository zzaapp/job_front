import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';
import { UserService } from '@shared/services/user.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { CompanyModel } from '@shared/models/company.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

    // Breadcrumb Info
    public pageTitle: string = 'Company';
    public breadcrumbs: any[] = [
        {title: 'Home', route: '/'},
        {title: 'Company', route: '/user/company'},
    ];

    // Modals
    bsModalRef: BsModalRef;

    // Company Form
    public companyForm: FormGroup;
    public title:       FormControl;
    public description: FormControl;
    public evolution:   FormControl;
    public current:     FormControl;

    // Alert Message
    public alert: AlertMessage = new AlertMessage();

    constructor(private httpService: HttpService,
                private modalService: BsModalService) {}

    ngOnInit() {
        this.buildControls();
        this.buildForm();

        this.getCompanyInfo();
    }

    private getCompanyInfo() {
        this.httpService.getProviderInfo().subscribe(
            (data: CompanyModel) => {
                console.log(data);
                this.populateForm(data);
            },
            (error: HttpErrorResponse) => {}
        );
    }

    private buildControls() {
        this.title       = new FormControl('', [Validators.required]);
        this.description = new FormControl('', [Validators.required]);
        this.evolution   = new FormControl('', []);
        this.current     = new FormControl('', []);
    }

    private buildForm() {
        this.companyForm = new FormGroup({
            title:       this.title,
            description: this.description,
            evolution:   this.evolution,
            current:     this.current
        });
    }

    private populateForm(companyInfo: CompanyModel) {
        this.title.setValue(companyInfo.company_name);
        this.description.setValue(companyInfo.company_description);
        this.evolution.setValue(companyInfo.company_evolution);
        this.current.setValue(companyInfo.company_current);
    }

    public canSubmit() {
        return this.companyForm.valid;
    }

    public onCloseAlert() {
        this.alert.reset();
    }

    public onSubmitForm() {
        this.alert.reset();

        this.httpService.updateProviderInfo(this.companyForm.value.title,
                                            this.companyForm.value.description,
                                            this.companyForm.value.evolution,
                                            this.companyForm.value.current).subscribe(
            (data: CompanyModel) => {
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Company informations saved.');
                this.showAlertModal(this.alert);
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;    
                switch (status) {
                case 400:
                    const errorKey = HelperService.getObjectFirstKey(error.error);
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Company informations error.');
                    break;
                case 403:
                case 404:
                case 500:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
                this.showAlertModal(this.alert);
            }
        );

    }

    private showAlertModal(alert: AlertMessage) {
        const initialState = {
            alert: alert
        };

        switch (alert.type) {
        case AlertMessage.TYPE_ERROR:
            this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
            break;
        case AlertMessage.TYPE_SUCCESS:
            this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
            break;
        }

    }

}