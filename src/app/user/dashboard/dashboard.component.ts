import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { isNullOrUndefined } from 'util';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { ApplicationModel } from '@shared/models/application.model';
import { SalaryModel } from '@shared/models/salary.model';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  // Breadcrumb Info
  public title: string = 'Dashboard';
  public breadcrumbs: any[] = [
    {title: 'Home', route: '/'},
    {title: 'Dashboard', route: '/user/dashboard'},
  ];

  // Variables
  public applications: ApplicationModel[];
  public estimatedSalary: number;

  constructor(private httpService: HttpService) {}

  ngOnInit() {
    this.getUserApplications();
    this.getUserSalaryEstimation();
  }

  public getUserApplications() {

    this.httpService.getUserApplications().subscribe(
        (data: ApplicationModel[]) => {
          
          this.applications = [];
          data.forEach(element => {
              this.applications.push(new ApplicationModel(element));
          });

        },
        (error: HttpErrorResponse) => {
          this.applications = [];
        }
    );

  }

  public getUserSalaryEstimation() {

    this.httpService.getUserSalary().subscribe(
      (data: SalaryModel) => {
        
        if( isNullOrUndefined(data) ) {
          this.estimatedSalary = null;
        } else {
          this.estimatedSalary = data.salary;
        }

      },
      (error: HttpErrorResponse) => {
        this.estimatedSalary = null;
      }
  );

  }

}