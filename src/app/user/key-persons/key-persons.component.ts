import { Component, OnInit, OnDestroy } from '@angular/core';

// Services
import { UserService } from '@shared/services/user.service';

// Models
import { KeyPersonModel } from '@shared/models/key-person.model';

@Component({
  selector: 'app-user-key-persons',
  templateUrl: './key-persons.component.html',
  styleUrls: ['./key-persons.component.css']
})
export class KeyPersonsComponent {

  // Breadcrumb Info
  public title: string = 'Company Key Persons';
  public breadcrumbs: any[] = [
    {title: 'Home', route: '/'},
    {title: 'Key Persons', route: '/user/key-persons'},
  ];

  constructor(private userService: UserService) {}

}