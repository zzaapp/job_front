import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute, Event, NavigationEnd } from '@angular/router';
import {isNullOrUndefined} from 'util';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';
import { NotifyService } from '@shared/services/notify.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { KeyPersonModel } from '@shared/models/key-person.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-user-key-persons-form',
  templateUrl: './key-persons-form.component.html',
  styleUrls: ['./key-persons-form.component.css']
})
export class KeyPersonsFormComponent implements OnInit {

  // Modals
  bsModalRef: BsModalRef;

  // Key Person Form
  public keyPersonForm: FormGroup;
  public name:          FormControl;
  public position:      FormControl;
  public file:          any;

  // Alert Message
  public alert: AlertMessage = new AlertMessage();

  constructor(private httpService: HttpService,
              private modalService: BsModalService,
              private notifyService: NotifyService) {}

  ngOnInit() {
    this.buildControls();
    this.buildForm();
  }

  private buildControls() {
    this.name       = new FormControl('', [Validators.required]);
    this.position   = new FormControl('', [Validators.required]);
  }

  private buildForm() {
    this.keyPersonForm = new FormGroup({
        name:     this.name,
        position: this.position
    });
  }

  public canSubmit() {
    return this.keyPersonForm.valid;
  }

  public onCloseAlert() {
    this.alert.reset();
  }

  public onFileChange(event) {
    if(event.target.files.length > 0) {
      this.file = event.target.files[0];
    }
  }

  public onSubmitForm() {
    this.alert.reset();

    if( isNullOrUndefined(this.file) ) {
        this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'You must upload an image');
        this.showAlertModal(this.alert);
    }

    this.httpService.keyPersonCreate(this.keyPersonForm.value.name,
                                     this.keyPersonForm.value.position,
                                     this.file).subscribe(
        (data: KeyPersonModel) => {
            this.notifyService.newKeyPerson.emit(data);
            this.resetForm();
            this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Key person successfully created');
            this.showAlertModal(this.alert);
        },
        (error: HttpErrorResponse) => {
            const status    = error.status;    
            switch (status) {
            case 400:
                const errorKey = HelperService.getObjectFirstKey(error.error);
                this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Key person error.');
                break;
            case 403:
            case 404:
            case 500:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                break;
            default:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                break;
            }
            this.showAlertModal(this.alert);
        }
    );

  }

  private showAlertModal(alert: AlertMessage) {
    const initialState = {
      alert: alert
    };

    switch (alert.type) {
    case AlertMessage.TYPE_ERROR:
        this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
        break;
    case AlertMessage.TYPE_SUCCESS:
        this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
        break;
    }

  }

  private resetForm() {
    this.keyPersonForm.reset();
  }

}