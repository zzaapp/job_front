import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent {

    // Breadcrumb Info
    public title: string = 'Faq Page';
    public breadcrumbs: any[] = [
        {title: 'Home', route: '/'},
        {title: 'Faq', route: ''},
    ];

}