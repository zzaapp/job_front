import { Component, OnInit, OnDestroy, Input, Output ,EventEmitter } from '@angular/core';

// Models
import { JobModel } from '@shared/models/job.model';

@Component({
  selector: 'app-visitor-job-box',
  templateUrl: './job-box.component.html',
  styleUrls: ['./job-box.component.css']
})
export class JobBoxComponent implements OnInit {

    // Inputs
    @Input() job: JobModel;

    constructor() {}

    ngOnInit() {}

}