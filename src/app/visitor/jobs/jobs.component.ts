import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute, Event, NavigationEnd } from '@angular/router';
import { isNullOrUndefined } from 'util';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { JobModel } from '@shared/models/job.model';
import { PaginatorRequestModel } from '@shared/models/paginator-request.model';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent {

  // Pagination & Filters
  public currentPage: number = 1;
  public lastPage:    number = 1;
  public filters:     any    = {};

  // Jobs
  public jobs: JobModel[] = [];

  constructor(private httpService: HttpService,
              private route: ActivatedRoute) {}

  ngOnInit() {
    this.initSubscriptions();
  }

  private getJobs(page: number) {
    this.currentPage = page;
    const paginator = new PaginatorRequestModel({page: page, filter: this.filters});
    this.httpService.getJobs(paginator).subscribe(
        (data: any[]) => {
            this.lastPage = data['last_page'];
            this.jobs     = data['data'];
        },
        (error: HttpErrorResponse) => {
            this.jobs = [];
        }
    );
  }

  public changePage(page: number) {
    this.getJobs(page);
  }

  private initSubscriptions() {

    // Change Query Params Subscription
    this.route.queryParamMap.subscribe(queryParams => {
      const search = queryParams.get("search");
      const category = queryParams.get("category");
      
      this.filters = {};
      if( !isNullOrUndefined(search) && search !== '' ) {
        this.filters.search = search;
      }
      if( !isNullOrUndefined(category) && category !== '' ) {
        this.filters.category = category;
      }

      this.getJobs(1);
    });

  }

}