import { Component, OnInit, OnDestroy, Input, Output ,EventEmitter } from '@angular/core';

// Models
import { ProviderModel } from '@shared/models/provider.model';
import { SocialModel } from '@shared/models/social.model';

@Component({
  selector: 'app-visitor-recruiter-box',
  templateUrl: './recruiter-box.component.html',
  styleUrls: ['./recruiter-box.component.css']
})
export class RecruiterBoxComponent implements OnInit {

    // Inputs
    @Input() provider: ProviderModel;
    @Input() providerSocials: SocialModel;

    constructor() {}

    ngOnInit() {}

}