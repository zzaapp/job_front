import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';
import { UserService } from '@shared/services/user.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { CompanyModel } from '@shared/models/company.model';
import { ReviewModel } from '@shared/models/review.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
    selector: 'app-visitor-job-reviews',
    templateUrl: './job-reviews.component.html',
    styleUrls: ['./job-reviews.component.css']
})
export class JobReviewsComponent {

  // Inputs
  @Input() company: CompanyModel;
  @Input() reviews: ReviewModel[];

  // Outputs
  @Output() public reviewAdd = new EventEmitter<ReviewModel>();

  // Modals
  bsModalRef: BsModalRef;

  // Review Form
  public reviewForm: FormGroup;
  public rating:     FormControl;
  public comment:    FormControl;

  // Alert Message
  public alert: AlertMessage = new AlertMessage();

  constructor(private httpService: HttpService,
              private modalService: BsModalService,
              private userService: UserService) {}

  ngOnInit() {
    this.buildControls();
    this.buildForm();
  }

  private buildControls() {
    this.rating  = new FormControl('1');
    this.comment = new FormControl('', [Validators.required]);
  }

  private buildForm() {
    this.reviewForm = new FormGroup({
        rating:  this.rating,
        comment: this.comment
    });
  }

  public canSubmit() {
    return this.reviewForm.valid;
  }

  public onCloseAlert() {
    this.alert.reset();
  }

  public onSubmitForm() {
    this.alert.reset();

    this.httpService.reviewCreate(this.reviewForm.value.rating,
                                  this.reviewForm.value.comment,
                                  this.company.id
                                  ).subscribe(
      (data: ReviewModel) => {
        this.addReview(data);
        this.resetForm();
        this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Your review has been submitted.');
        this.showAlertModal(this.alert);
      },
      (error: HttpErrorResponse) => {
        const status    = error.status;    
        switch (status) {
          case 400:
            const errorKey = HelperService.getObjectFirstKey(error.error);
            this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Review error.');
            break;
          case 403:
          case 404:
          case 424:
          case 500:
            this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
            break;
          default:
            this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
            break;
        }
        this.showAlertModal(this.alert);
      }
    );

  }

  private addReview(data: ReviewModel) {
    const newReview = new ReviewModel({
        'rating': data.rating,
        'comment': data.comment,
        'reviewer': this.userService.getUser()
    });
    this.reviews.push(newReview);
    this.reviewAdd.emit(newReview);
  }

  private showAlertModal(alert: AlertMessage) {
    const initialState = {
      alert: alert
    };

    switch (alert.type) {
      case AlertMessage.TYPE_ERROR:
        this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
        break;
      case AlertMessage.TYPE_SUCCESS:
        this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
        break;
    }

  }

  private resetForm() {
    this.reviewForm.reset();
    this.rating.setValue('1');
  }

}