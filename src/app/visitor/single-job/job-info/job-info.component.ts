import { Component, OnInit, OnDestroy, Input, Output ,EventEmitter } from '@angular/core';

// Models
import { JobModel } from '@shared/models/job.model';
import { JobImageModel } from '@shared/models/job-image.model';
import { CompanyModel } from '@shared/models/company.model';
import { ReviewModel } from '@shared/models/review.model';
import { KeyPersonModel } from '@shared/models/key-person.model';

@Component({
  selector: 'app-visitor-job-info',
  templateUrl: './job-info.component.html',
  styleUrls: ['./job-info.component.css']
})
export class JobInfoComponent implements OnInit {

    // Inputs
    @Input() job: JobModel;
    @Input() images: JobImageModel;
    @Input() company: CompanyModel;
    @Input() reviews: ReviewModel[];
    @Input() keyPersons: KeyPersonModel[];

    constructor() {}

    ngOnInit() {}

    public addReview(review: ReviewModel) {
      this.reviews.push(review);
    }

}