import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { isNullOrUndefined } from 'util';
import { Router, ActivatedRoute, Event, NavigationEnd } from '@angular/router';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';
import { JobModel } from '@shared/models/job.model';
import { ProviderModel } from '@shared/models/provider.model';
import { SocialModel } from '@shared/models/social.model';
import { JobImageModel } from '@shared/models/job-image.model';
import { CompanyModel } from '@shared/models/company.model';
import { ReviewModel } from '@shared/models/review.model';
import { KeyPersonModel } from '@shared/models/key-person.model';

@Component({
  selector: 'app-single-job',
  templateUrl: './single-job.component.html',
  styleUrls: ['./single-job.component.css']
})
export class SingleJobComponent {

  // Breadcrumb Info
  public title: string = '';
  public breadcrumbs: any[] = [
    {title: 'Home', route: '/'},
    {title: 'Single Job', route: ''},
  ];

  // Display Variables
  public job: JobModel;
  public provider: ProviderModel;
  public providerSocials: SocialModel;
  public images: JobImageModel[];
  public company: CompanyModel;
  public reviews: ReviewModel[];
  public keyPersons: KeyPersonModel[];

  // Job Id
  private jobId: number;

  constructor(private activatedRoute: ActivatedRoute,
              private httpService: HttpService,
              private router: Router) {}

  ngOnInit() {
    // Get id from URL
    this.jobId = +this.activatedRoute.snapshot.url[1].path;

    this.getJob(this.jobId);
  }

  private getJob(jobId: number) {
    
    this.httpService.getJob(jobId).subscribe(
        (data: any[]) => {
          this.job             = new JobModel(data['job']);
          this.provider        = new ProviderModel(data['user']);
          this.providerSocials = new SocialModel(data['user_socials'][0]);
          this.images          = data['job_images'];
          this.company         = new CompanyModel(data['company']);
          this.reviews         = data['reviews'];
          this.keyPersons      = data['key_persons'];

          this.title = this.job.title;
        },
        (error: HttpErrorResponse) => {
            this.router.navigate(['/jobs']);
        }
    );

  }

}