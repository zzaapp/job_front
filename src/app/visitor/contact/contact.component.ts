import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute } from '@angular/router';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {

  // Breadcrumb Info
  public title: string = 'Contact Us';
  public breadcrumbs: any[] = [
    {title: 'Home', route: '/'},
    {title: 'Contact Us', route: '/contact'},
  ];

  // Modals
  bsModalRef: BsModalRef;

  // Contact Form
  public contactForm: FormGroup;
  public email:       FormControl;
  public name:        FormControl;
  public message:     FormControl;

  // Alert Message
  public alert: AlertMessage = new AlertMessage();

  constructor(private httpService: HttpService,
              private modalService: BsModalService,
              private router: Router) {}

  ngOnInit() {
    this.buildControls();
    this.buildForm();
  }

  private buildControls() {
    this.email   = new FormControl('', [Validators.required]);
    this.name    = new FormControl('');
    this.message = new FormControl('', [Validators.required]);
  }

  private buildForm() {
    this.contactForm = new FormGroup({
      email:   this.email,
      name:    this.name,
      message: this.message,
    });
  }

  public canSubmit() {
    return this.contactForm.valid;
  }

  public onCloseAlert() {
    this.alert.reset();
  }

  public onSubmitForm() {
    this.alert.reset();

    this.httpService.contactSend(this.contactForm.value.email,
                                 this.contactForm.value.name,
                                 this.contactForm.value.message).subscribe(
      (data: any) => {
        this.resetForm();
        this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Your message has been sent.');
        this.showAlertModal(this.alert);
      },
      (error: HttpErrorResponse) => {
        const status    = error.status;    
        switch (status) {
          case 400:
            const errorKey = HelperService.getObjectFirstKey(error.error);
            this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Contact error.');
            break;
          case 403:
          case 404:
          case 500:
            this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
            break;
          default:
            this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
            break;
        }
        this.showAlertModal(this.alert);
      }
    );

  }

  private showAlertModal(alert: AlertMessage) {
    const initialState = {
      alert: alert
    };

    switch (alert.type) {
      case AlertMessage.TYPE_ERROR:
        this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
        break;
      case AlertMessage.TYPE_SUCCESS:
        this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
        break;
    }

  }

  private resetForm() {
    this.contactForm.reset();
  }

}