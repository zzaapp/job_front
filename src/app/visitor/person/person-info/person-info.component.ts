import { Component, OnInit, OnDestroy, Input, Output ,EventEmitter } from '@angular/core';

// Models
import { ProviderModel } from '@shared/models/provider.model';
import { UserInfoModel } from '@shared/models/user-info.model';

@Component({
  selector: 'app-person-info',
  templateUrl: './person-info.component.html',
  styleUrls: ['./person-info.component.css']
})
export class PersonInfoComponent implements OnInit {

    // Inputs
    @Input() person: ProviderModel;
    @Input() personInfo: UserInfoModel;
    @Input() shownBox: string;

    constructor() {}

    ngOnInit() {}

}