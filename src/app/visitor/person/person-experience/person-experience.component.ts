import { Component, OnInit, OnDestroy, Input, Output ,EventEmitter } from '@angular/core';

// Models
import { CvJobModel } from '@shared/models/cv-job.model';

@Component({
  selector: 'app-person-experience',
  templateUrl: './person-experience.component.html',
  styleUrls: ['./person-experience.component.css']
})
export class PersonExperienceComponent implements OnInit {

    // Inputs
    @Input() cvJobs: CvJobModel[];
    @Input() shownBox: string;

    constructor() {}

    ngOnInit() {}

}