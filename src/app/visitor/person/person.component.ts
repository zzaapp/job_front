import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { isNullOrUndefined } from 'util';
import { Router, ActivatedRoute, Event, NavigationEnd } from '@angular/router';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { ProviderModel } from '@shared/models/provider.model';
import { UserInfoModel } from '@shared/models/user-info.model';
import { SocialModel } from '@shared/models/social.model';
import { CvEducationModel } from '@shared/models/cv-education.model';
import { CvJobModel } from '@shared/models/cv-job.model';


@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent {

  // Breadcrumb Info
  public title: string = '';
  public breadcrumbs: any[] = [
    {title: 'Home', route: '/'},
    {title: 'Person', route: ''},
  ];

  // Display Variables
  public person:     ProviderModel;
  public socials:    SocialModel;
  public personInfo: UserInfoModel;
  public cvEducation: CvEducationModel[];
  public cvJobs: CvJobModel[];
  public shownBox: string = 'profile';

  // Person Id
  private personId: string;

  constructor(private activatedRoute: ActivatedRoute,
              private httpService: HttpService,
              private router: Router) {}

  ngOnInit() {
    // Get id from URL
    this.personId = this.activatedRoute.snapshot.url[1].path;

    this.getPerson(this.personId);
  }

  private getPerson(personId: string) {
    
    this.httpService.getPerson(personId).subscribe(
        (data: any[]) => {
          this.person      = new ProviderModel(data['person']);
          this.title       = this.person.first_name + ' ' + this.person.last_name; 
          this.socials     = new SocialModel(data['user_socials']);
          this.personInfo  = new UserInfoModel(data['person_info']);
          this.cvEducation = data['cv_education'];
          this.cvJobs      = data['cv_jobs'];
        },
        (error: HttpErrorResponse) => {
            this.router.navigate(['/']);
        }
    );

  }

  public showBox(box: string) {
    this.shownBox = box;
  }

}