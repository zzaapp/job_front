import { Component, OnInit, OnDestroy, Input, Output ,EventEmitter } from '@angular/core';

// Models
import { ProviderModel } from '@shared/models/provider.model';
import { UserInfoModel } from '@shared/models/user-info.model';
import { SocialModel } from '@shared/models/social.model';

@Component({
  selector: 'app-person-summary',
  templateUrl: './person-summary.component.html',
  styleUrls: ['./person-summary.component.css']
})
export class PersonSummaryComponent implements OnInit {

    // Inputs
    @Input() person: ProviderModel;
    @Input() socials: SocialModel;
    @Input() personInfo: UserInfoModel;

    constructor() {}

    ngOnInit() {}

}