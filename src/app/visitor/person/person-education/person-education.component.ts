import { Component, OnInit, OnDestroy, Input, Output ,EventEmitter } from '@angular/core';

// Models
import { CvEducationModel } from '@shared/models/cv-education.model';

@Component({
  selector: 'app-person-education',
  templateUrl: './person-education.component.html',
  styleUrls: ['./person-education.component.css']
})
export class PersonEducationComponent implements OnInit {

    // Inputs
    @Input() cvEducation: CvEducationModel[];
    @Input() shownBox: string;

    constructor() {}

    ngOnInit() {}

}