import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { VisitorRoutingModule } from './visitor-routing.module';
import { SharedModule } from '@shared/shared.module';

// Components
import { HomepageComponent } from './homepage/homepage.component';
import { JobsComponent } from './jobs/jobs.component';
import { JobBoxComponent } from './jobs/job-box/job-box.component';
import { SingleJobComponent } from './single-job/single-job.component';
import { JobInfoComponent } from './single-job/job-info/job-info.component';
import { JobReviewsComponent } from './single-job/job-info/job-reviews/job-reviews.component';
import { RecruiterBoxComponent } from './single-job/recruiter-box/recruiter-box.component'
import { ApplyBoxComponent } from './single-job/apply-box/apply-box.component';
import { ContactComponent } from './contact/contact.component';
import { PersonComponent } from './person/person.component';
import { PersonSummaryComponent } from './person/person-summary/person-summary.component';
import { PersonInfoComponent } from './person/person-info/person-info.component';
import { PersonEducationComponent } from './person/person-education/person-education.component';
import { PersonExperienceComponent } from './person/person-experience/person-experience.component';
import { FaqComponent } from './faq/faq.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { HowItWorksComponent } from './how-it-works/how-it-works.component';

@NgModule({
  imports: [
      CommonModule,
      VisitorRoutingModule,
      SharedModule,
      HttpClientModule
  ],
  exports: [
    VisitorRoutingModule,
  ],
  declarations: [
    HomepageComponent,
    JobsComponent,
    JobBoxComponent,
    SingleJobComponent,
    JobInfoComponent,
    JobReviewsComponent,
    RecruiterBoxComponent,
    ApplyBoxComponent,
    ContactComponent,
    PersonComponent,
    PersonSummaryComponent,
    PersonInfoComponent,
    PersonEducationComponent,
    PersonExperienceComponent,
    FaqComponent,
    TermsConditionsComponent,
    HowItWorksComponent
  ]
})
export class VisitorModule { }
