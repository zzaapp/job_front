import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { HomepageComponent } from './homepage/homepage.component';
import { JobsComponent } from './jobs/jobs.component';
import { SingleJobComponent } from './single-job/single-job.component';
import { ContactComponent } from './contact/contact.component';
import { PersonComponent } from './person/person.component';
import { FaqComponent } from './faq/faq.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { HowItWorksComponent } from './how-it-works/how-it-works.component';

export const visitorRoutes: Routes = [
    { path: '', component: HomepageComponent },
    { path: 'jobs', component: JobsComponent },
    { path: 'job/:id', component: SingleJobComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'person/:id', component: PersonComponent },
    { path: 'faq', component: FaqComponent },
    { path: 'terms-conditions', component: TermsConditionsComponent },
    { path: 'how-it-works', component: HowItWorksComponent }
];
@NgModule({
    imports: [
        RouterModule.forChild(visitorRoutes),
    ],
    exports: [
        RouterModule,
    ],
    declarations: [
    ]
})
export class VisitorRoutingModule {}
