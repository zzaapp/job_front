import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.css']
})
export class TermsConditionsComponent {

    // Breadcrumb Info
    public title: string = 'Terms & Conditions';
    public breadcrumbs: any[] = [
        {title: 'Home', route: '/'},
        {title: 'Terms & Conditions', route: ''},
    ];

}