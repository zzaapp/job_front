import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-how-it-works',
  templateUrl: './how-it-works.component.html',
  styleUrls: ['./how-it-works.component.css']
})
export class HowItWorksComponent {

    // Breadcrumb Info
    public title: string = 'How It Works';
    public breadcrumbs: any[] = [
        {title: 'Home', route: '/'},
        {title: 'How It Works', route: ''},
    ];

}