import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ActivationComponent } from './activation/activation.component';

// Guards
import { GuestGuard } from '@shared/guards/guest.guard';

export const guestRoutes: Routes = [
    { path: 'login', component: LoginComponent, canActivate: [GuestGuard] },
    { path: 'register', component: RegisterComponent, canActivate: [GuestGuard] },
    { path: 'activation/:token', component: ActivationComponent, canActivate: [GuestGuard] }
];
@NgModule({
    imports: [
        RouterModule.forChild(guestRoutes),
    ],
    exports: [
        RouterModule,
    ],
    declarations: [
    ]
})
export class GuestRoutingModule {}
