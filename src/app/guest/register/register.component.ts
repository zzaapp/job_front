import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  // Breadcrumb Info
  public title: string = 'Register';
  public breadcrumbs: any[] = [
    {title: 'Home', route: '/'},
    {title: 'Register', route: '/register'},
  ];

  // Modals
  bsModalRef: BsModalRef;

  // Register Form
  public registerForm:    FormGroup;
  public firstName:       FormControl;
  public lastName:        FormControl;
  public email:           FormControl;
  public password:        FormControl;
  public confirmPassword: FormControl;

  // Alert Message
  public alert: AlertMessage = new AlertMessage();

  constructor(private httpService: HttpService,
              private modalService: BsModalService) {}

  ngOnInit() {
    this.buildControls();
    this.buildForm();
  }

  private buildControls() {
    this.firstName       = new FormControl('', [Validators.required]);
    this.lastName        = new FormControl('', [Validators.required]);
    this.email           = new FormControl('', [Validators.required]);
    this.password        = new FormControl('', [Validators.required]);
    this.confirmPassword = new FormControl('', [Validators.required]);
  }

  private buildForm() {
    this.registerForm = new FormGroup({
      firstName:       this.firstName,
      lastName:        this.lastName,
      email:           this.email,
      password:        this.password,
      confirmPassword: this.confirmPassword
    });
  }

  public canSubmit() {
    return this.registerForm.valid;
  }

  public onCloseAlert() {
    this.alert.reset();
  }

  public onSubmitForm() {
    this.alert.reset();

    this.httpService.registerUser(this.registerForm.value.firstName,
                                  this.registerForm.value.lastName,
                                  this.registerForm.value.email,
                                  this.registerForm.value.password,
                                  this.registerForm.value.confirmPassword).subscribe(
      (data: any) => {
        this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'An activation email has been sent.');
        this.showAlertModal(this.alert);
      },
      (error: HttpErrorResponse) => {
        const status    = error.status;    
        switch (status) {
          case 400:
            const errorKey = HelperService.getObjectFirstKey(error.error);
            this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Register error.');
            break;
          case 403:
          case 404:
          case 500:
            this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
            break;
          default:
            this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
            break;
        }
        this.showAlertModal(this.alert);
      }
    );

  }

  private showAlertModal(alert: AlertMessage) {
    const initialState = {
      alert: alert
    };

    switch (alert.type) {
      case AlertMessage.TYPE_ERROR:
        this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
        break;
      case AlertMessage.TYPE_SUCCESS:
        this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
        break;
    }

  }
  
}