import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { GuestRoutingModule } from './guest-routing.module';
import { SharedModule } from '@shared/shared.module';

// Components
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ActivationComponent } from './activation/activation.component';

// Guards
import { GuestGuard } from '@shared/guards/guest.guard';

@NgModule({
  imports: [
    CommonModule,
    GuestRoutingModule,
    SharedModule,
    HttpClientModule
  ],
  exports: [
    GuestRoutingModule,
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    ActivationComponent
  ],
  providers: [
    GuestGuard
  ]
})
export class GuestModule { }
