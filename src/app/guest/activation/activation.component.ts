import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute, Event, NavigationEnd } from '@angular/router';

// Services
import { HttpService } from '@shared/services/http.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';

@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.css']
})
export class ActivationComponent implements OnInit {

    // Breadcrumb Info
    public title: string = 'Activation';
    public breadcrumbs: any[] = [
        {title: 'Home', route: '/'},
        {title: 'Activation', route: '/activation'},
    ];

    // Activation token
    private token: string;

    // Alert Message
    public alert: AlertMessage = new AlertMessage();

    constructor( private router: Router,
                 private activatedRoute: ActivatedRoute,
                 private httpService: HttpService
               ) {}

    ngOnInit() {
        // Get token from URL
        this.token = this.activatedRoute.snapshot.url[1].path;

        this.httpService.activateUser(this.token).subscribe(
            (any: any) => {
                this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS , 'Your user is now activated.');
            },
            (error: HttpErrorResponse) => {
                const status    = error.status;
                switch (status) {
                case 400:
                    this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error ? error.error.error : 'Activation error.');
                    break;
                default:
                    this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                    break;
                }
            }
        );

    }

}