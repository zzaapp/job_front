import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute } from '@angular/router';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';
import { UserService } from '@shared/services/user.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // Breadcrumb Info
  public title: string = 'Login';
  public breadcrumbs: any[] = [
    {title: 'Home', route: '/'},
    {title: 'Login', route: '/login'},
  ];

  // Modals
  bsModalRef: BsModalRef;

  // Login Form
  public loginForm: FormGroup;
  public email:     FormControl;
  public password:  FormControl;

  // Alert Message
  public alert: AlertMessage = new AlertMessage();

  constructor(private httpService: HttpService,
              private modalService: BsModalService,
              private router: Router,
              private userService: UserService) {}

  ngOnInit() {
    this.buildControls();
    this.buildForm();
  }

  private buildControls() {
    this.email           = new FormControl('', [Validators.required]);
    this.password        = new FormControl('', [Validators.required]);
  }

  private buildForm() {
    this.loginForm = new FormGroup({
      email:    this.email,
      password: this.password
    });
  }

  public canSubmit() {
    return this.loginForm.valid;
  }

  public onCloseAlert() {
    this.alert.reset();
  }

  public onSubmitForm() {
    this.alert.reset();

    this.httpService.loginUser(this.loginForm.value.email,
                               this.loginForm.value.password).subscribe(
      (data: any) => {
        this.userService.login(data);
        this.router.navigate(['user/dashboard']);
      },
      (error: HttpErrorResponse) => {
        const status    = error.status;    
        switch (status) {
          case 400:
            const errorKey = HelperService.getObjectFirstKey(error.error);
            this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Login error.');
            break;
          case 403:
          case 404:
          case 500:
            this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
            break;
          default:
            this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
            break;
        }
        this.showAlertModal(this.alert);
      }
    );

  }

  private showAlertModal(alert: AlertMessage) {
    const initialState = {
      alert: alert
    };

    switch (alert.type) {
      case AlertMessage.TYPE_ERROR:
        this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
        break;
      case AlertMessage.TYPE_SUCCESS:
        this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
        break;
    }

  }

}