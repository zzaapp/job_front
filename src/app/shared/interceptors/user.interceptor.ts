import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpEvent } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { switchMap, take, catchError } from 'rxjs/operators';
import { UserService } from '@shared/services/user.service';

@Injectable()
export class UserInterceptor implements HttpInterceptor {

    public constructor(private injector: Injector, private router: Router) {}

    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const userService = this.injector.get(UserService);

        let req = null;
        if (userService.isAuthenticated()) {
            req = request.clone({headers: request.headers.set('Authorization', 'Bearer ' + userService.getAccessToken())});
        } else {
            req = request.clone();
        }

        return next.handle(req).pipe(
            catchError(
                (error: HttpErrorResponse) => {
                    const status = error.status;
                    switch (status) {
                        case 401:
                            userService.logout();
                            this.router.navigate(['login']);
                            break;
                        case 498:
                            userService.logout();
                            this.router.navigate(['login']);
                            break;
                    }
                    console.log(error.status);
                    return throwError(error);
                }
            )
        );

    }
}
