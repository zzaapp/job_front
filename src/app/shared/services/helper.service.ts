import {Injectable} from '@angular/core';
import {isNullOrUndefined} from 'util';

@Injectable({
    providedIn: 'root'
})
export class HelperService {

    static isNullOrUndefined(value: any): boolean {
        return value === null || value === undefined;
    }

    static getObjectFirstKey(object: any): any {
        if (typeof object === 'object') {
            for (const key in object) {
                return object[key];
            }
        }
        return null;
    }

   static findObjectIndexByCriteria(items: any[], by: string, value: any, insensitive: boolean = false): any {
       if (items.length) {
           if (insensitive) {
               for (let i = 0; i < items.length; i++) {
                   if (!isNullOrUndefined(items[i]) && !isNullOrUndefined(items[i][by]) && items[i][by].toLowerCase() == value.toLowerCase()) {
                       return i;
                   }
               }
           } else {
               for (let i = 0; i < items.length; i++) {
                   if (!isNullOrUndefined(items[i]) && !isNullOrUndefined(items[i][by]) && items[i][by] == value) {
                       return i;
                   }
               }
           }
       }
       return null;
   }

    /**
     * Complex Object to query
     * @param obj
     * @param ignoreNull
     * @param prefix
     */
    static complexObjectToQuery(obj: any, ignoreNull: boolean = true, prefix: string = null): string {
        const fields = [];
        if (obj && Object.keys(obj).length) {
            for (const i in obj) {
                if (obj.hasOwnProperty(i)) {
                    if (typeof obj[i] === 'object') {
                        fields.push(HelperService.complexObjectToQuery(obj[i], ignoreNull));
                    } else {
                        if (ignoreNull && obj[i] === null) {
                            continue;
                        }
                        fields.push(
                            (prefix ? prefix + '[' + i + ']' : i) +
                            '=' +
                            (typeof obj[i] === 'undefined' ? '' : encodeURIComponent(obj[i]))
                        );
                    }
                }
            }
        }
        return fields.join('&');
    }

}