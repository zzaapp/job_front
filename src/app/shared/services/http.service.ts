import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {isNullOrUndefined} from 'util';

// Services
import { UserService } from '@shared/services/user.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { SocialModel } from '@shared/models/social.model';
import { UserInfoModel } from '@shared/models/user-info.model';
import { JobModel } from '@shared/models/job.model';
import { PaginatorRequestModel } from '@shared/models/paginator-request.model';
import { ApplicationModel } from '@shared/models/application.model';
import { AppointmentModel } from '@shared/models/appointment.model';
import { JobImageModel } from '@shared/models/job-image.model';
import { CompanyModel } from '@shared/models/company.model';
import { ReviewModel } from '@shared/models/review.model';
import { KeyPersonModel } from '@shared/models/key-person.model';
import { CvJobModel } from '@shared/models/cv-job.model';
import { CvEducationModel } from '@shared/models/cv-education.model';
import { CareerInfoModel } from '@shared/models/career-info.model';
import { SalaryModel } from '@shared/models/salary.model';

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    private apiURL: string = 'http://localhost/jobfull/jobfull_api/public'; //https://api.getjobb.eu/public

    constructor(private http: HttpClient,
                private userService: UserService) {}

    public registerUser(firstName: string, lastName: string,
                        email: string, password: string,
                        confirmPassword: string) {

        const formData = {
            'email': email,
            'password': password,
            'confirmPassword': confirmPassword,
            'first_name': firstName,
            'last_name': lastName,
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.apiURL + '/users/create', formData, {headers: headers}).pipe(
            map((data: any) => {
                console.log('create user', data);
                return data;
            })
        );
             
    }

    public loginUser(email: string, password: string) {

        const formData = {
            'email': email,
            'password': password
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.apiURL + '/users/login', formData, {headers: headers}).pipe(
            map((data: any) => {
                console.log('login user', data);
                return data;
            })
        );
             
    }

    public activateUser(activationToken: string) {

        const formData = {
            'activation_token': activationToken
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.patch(this.apiURL + '/users/activate', formData, {headers: headers}).pipe(
            map((data: any) => {
                console.log('activate user', data);
                return data;
            })
        );
             
    }

    public logoutUser(userId: string) {

        const url = this.apiURL + '/users/' + userId + '/logout';
        const headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this.http.post(url, null, {headers: headers}).pipe(
            map((data: any) => {
                console.log('logout user', data);
                return data;
            })
        );

    }

    public subscribeToNewsletter(email: string) {

        const formData = {
            'email': email
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.apiURL + '/newsletter/create', formData, {headers: headers}).pipe(
            map((data: any) => {
                console.log('newsletter subscribe', data);
                return data;
            })
        );

    }

    public getSocialAccounts() {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/social/' + userId + '/socials';

        return this.http.get(url).pipe(
            map((data: SocialModel) => {
                console.log('user social accounts', data);
                return new SocialModel((data && data[0])? data[0]: null);
            })
        );

    }

    public updateSocialAccounts(facebook:  string,
                                instagram: string,
                                twitter:   string,
                                dribbble:  string) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/social/' + userId + '/update';

        const formData = {
            'facebook':  facebook,
            'instagram': instagram,
            'twitter':   twitter,
            'dribbble':  dribbble,
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(url, formData, {headers: headers}).pipe(
            map((data: SocialModel) => {
                console.log('user social accounts update', data);
                return new SocialModel((data && data[0])? data[0]: null);
            })
        );

    }

    public getUserInfo() {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/info/' + userId + '/info';

        return this.http.get(url).pipe(
            map((data: UserInfoModel) => {
                console.log('user info', data);
                return new UserInfoModel((data && data[0])? data[0]: null);
            })
        );

    }

    public updateUserInfo(firstName: string,
                          lastName: string,
                          country: string,
                          county: string,
                          address: string,
                          spokenLanguages: string,
                          hobbies: string,
                          curiosities: string) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/info/' + userId + '/update';

        const formData = {
            'first_name':       firstName,
            'last_name':        lastName,
            'country':          country,
            'county':           county,
            'address':          address,
            'spoken_languages': spokenLanguages,
            'hobbies':          hobbies,
            'curiosities':      curiosities
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(url, formData, {headers: headers}).pipe(
            map((data: UserInfoModel) => {
                console.log('user info update', data);
                return new UserInfoModel(data? data: null);
            })
        );

    }

    public providerCreate(phone: string,
                          companyName: string,
                          companyDescription: string,
                          companyEvolution?: string,
                          companyCurrent?: string) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/provider/' + userId + '/create';

        const formData = {
            'phone':               phone,
            'company_name':        companyName,
            'company_description': companyDescription,
            'company_evolution':   companyEvolution? companyEvolution : null,
            'company_current':     companyCurrent? companyCurrent : null
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(url, formData, {headers: headers}).pipe(
            map((data: CompanyModel) => {
                console.log('user provider create', data);
                return data;
            })
        );

    }

    public jobCreate(title: string,
                     category: string,
                     description: string,
                     included?: string,
                     duration?: number) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/jobs/' + userId + '/create';

        const formData = {
            'title':       title,
            'category':    category,
            'description': description,
            'included':    included? included : null,
            'duration':    duration? duration : null
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(url, formData, {headers: headers}).pipe(
            map((data: any) => {
                console.log('job create', data);
                return data;
            })
        );

    }

    public getUserJobs(jobId?: number) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/jobs/' + userId + '/jobs' + (!isNullOrUndefined(jobId)? '/'+jobId : '');

        return this.http.get(url).pipe(
            map((data: any[]) => {
                console.log('user jobs', data);
                return data;
            })
        );

    }

    public jobDelete(jobId: number) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/jobs/' + userId + '/' + jobId;

        return this.http.delete(url).pipe(
            map((data: any[]) => {
                console.log('user job deleted', data);
                return data;
            })
        );

    }

    public jobUpdate(jobId: number,
                     title: string,
                     category: string,
                     description: string,
                     included?: string,
                     duration?: number) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/jobs/' + userId + '/update/' + jobId;

        const formData = {
            'title':       title,
            'category':    category,
            'description': description,
            'included':    included? included : null,
            'duration':    duration? duration : null
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.put(url, formData, {headers: headers}).pipe(
            map((data: JobModel) => {
                console.log('job update', data);
                return data;
            })
        );

    }

    public jobChangeStatus(jobId: number,
                           active: boolean) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/jobs/' + userId + '/change-status/' + jobId;

        const formData = {
            'active': active
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.put(url, formData, {headers: headers}).pipe(
            map((data: JobModel) => {
                console.log('job change status', data);
                return data;
            })
        );

    }

    public getJobs(paginatorRequest: PaginatorRequestModel = null) {
        
        const query = paginatorRequest !== null ? '?' + HelperService.complexObjectToQuery(paginatorRequest) : '';
        const url = this.apiURL + '/jobs' + query;

        return this.http.get(url).pipe(
            map((data: any[]) => {
                console.log('jobs', data);
                return data;
            })
        );

    }

    public getJob(jobId: number) {

        const url = this.apiURL + '/jobs/' + jobId;

        return this.http.get(url).pipe(
            map((data: any[]) => {
                console.log('job', data);
                return data;
            })
        );

    }

    public applicationCreate(jobId: number) {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/applications/' + userId + '/create';

        const formData = {
            'job_id': jobId
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(url, formData, {headers: headers}).pipe(
            map((data: any) => {
                console.log('application create', data);
                return data;
            })
        );
    }

    public getUserApplications() {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/applications/' + userId + '/applications';

        return this.http.get(url).pipe(
            map((data: ApplicationModel[]) => {
                console.log('user applications', data);
                return data;
            })
        );
    }

    public applicationCancel(applicationId: number) {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/applications/' + userId + '/cancel/' + applicationId;

        const formData = {};

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.put(url, formData, {headers: headers}).pipe(
            map((data: ApplicationModel) => {
                console.log('application canceled', data);
                return data;
            })
        );
    }

    public getUserAppointments() {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/appointments/' + userId + '/appointments';

        return this.http.get(url).pipe(
            map((data: AppointmentModel[]) => {
                console.log('user appointments', data);
                return data;
            })
        );
    }

    public appointmentAccept(appointmentId: number) {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/appointments/' + userId + '/accept/' + appointmentId;

        const formData = {};

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.put(url, formData, {headers: headers}).pipe(
            map((data: ApplicationModel) => {
                console.log('appointment accepted', data);
                return data;
            })
        );
    }

    public appointmentDecline(appointmentId: number) {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/appointments/' + userId + '/decline/' + appointmentId;

        const formData = {};

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.put(url, formData, {headers: headers}).pipe(
            map((data: ApplicationModel) => {
                console.log('appointment declined', data);
                return data;
            })
        );
    }

    public uploadUserAvatar(file: any) {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/info/' + userId + '/upload-image';

        const formData = new FormData();
        formData.append('file', file);

        return this.http.post(url, formData).pipe(
            map((data: UserInfoModel) => {
                console.log('upload user avatar', data);
                return data;
            })
        );
    }

    public deleteUserAvatar() {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/info/' + userId + '/delete-image';

        return this.http.delete(url).pipe(
            map((data: any[]) => {
                console.log('user avatar deleted', data);
                return data;
            })
        );
    }

    public getPerson(personId: string) {
        const url = this.apiURL + '/persons/' + personId;

        return this.http.get(url).pipe(
            map((data: ApplicationModel[]) => {
                console.log('person', data);
                return data;
            })
        );
    }

    public contactSend(email: string,
                       name: string,
                       message: string) {

        const url = this.apiURL + '/contact/create';

        const formData = {
            'email':   email,
            'name':    name? name : null,
            'message': message
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(url, formData, {headers: headers}).pipe(
            map((data: any) => {
                console.log('contact create', data);
                return data;
            })
        );
    }

    public getJobImages(jobId: number) {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/jobs/images/' + userId + '/' + jobId + '/images';

        return this.http.get(url).pipe(
            map((data: JobImageModel[]) => {
                console.log('user job images', data);
                return data;
            })
        );
    }

    public uploadJobImage(jobId: number, file: any) {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/jobs/images/' + userId + '/upload/' + jobId;

        const formData = new FormData();
        formData.append('file', file);

        return this.http.post(url, formData).pipe(
            map((data: JobImageModel) => {
                console.log('upload job image', data);
                return data;
            })
        );
    }

    public deleteJobImage(jobId: number, imageId: number) {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/jobs/images/' + userId + '/' + jobId + '/' + imageId;

        return this.http.delete(url).pipe(
            map((data: JobImageModel) => {
                console.log('job image deleted', data);
                return data;
            })
        );
    }

    public getProviderInfo() {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/provider/' + userId + '/info';

        return this.http.get(url).pipe(
            map((data: CompanyModel[]) => {
                console.log('provider info', data[0]);
                return data[0];
            })
        );
    }

    public updateProviderInfo(title: string,
                              description: string,
                              evolution?: string,
                              current?: string) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/provider/' + userId + '/update';

        const formData = {
            'company_name':        title,
            'company_description': description,
            'company_evolution':   evolution? evolution : null,
            'company_current':     current? current : null
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.put(url, formData, {headers: headers}).pipe(
            map((data: CompanyModel) => {
                console.log('update provider data', data);
                return data;
            })
        );
    }

    public reviewCreate(rating: number,
                        comment: string,
                        providerId: number) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/provider/reviews/' + userId + '/' + providerId  +'/create';

        const formData = {
            'rating':  rating,
            'comment': comment
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(url, formData, {headers: headers}).pipe(
            map((data: ReviewModel) => {
                console.log('review create', data);
                return data;
            })
        );
    }

    public keyPersonCreate(name: string,
                           position: string,
                           file: any) {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/provider/persons/' + userId + '/create';

        const formData = new FormData();
        formData.append('file', file);
        formData.append('name', name);
        formData.append('position', position);

        return this.http.post(url, formData).pipe(
            map((data: KeyPersonModel) => {
                console.log('create key person', data);
                return data;
            })
        );
    }

    public getKeyPersons() {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/provider/persons/' + userId + '/persons';

        return this.http.get(url).pipe(
            map((data: KeyPersonModel[]) => {
                console.log('key persons get', data);
                return data;
            })
        );
    }

    public deleteKeyPerson(personId: number) {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/provider/persons/' + userId + '/' + personId;

        return this.http.delete(url).pipe(
            map((data: KeyPersonModel) => {
                console.log('key person deleted', data);
                return data;
            })
        );
    }

    public cvJobCreate(position: string,
                       company: string,
                       yearStart: number,
                       yearEnd: number,
                       responsibilities: string) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/cv/jobs/' + userId + '/create';

        const formData = {
            'position':         position,
            'company':          company,
            'year_start':       yearStart,
            'year_end':         yearEnd,
            'responsibilities': responsibilities,
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(url, formData, {headers: headers}).pipe(
            map((data: CvJobModel) => {
                console.log('cv job create', data);
                return data;
            })
        );
    }

    public cvJobsGet() {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/cv/jobs/' + userId + '/jobs';

        return this.http.get(url).pipe(
            map((data: CvJobModel[]) => {
                console.log('cv jobs get', data);
                return data;
            })
        );
    }

    public cvJobDelete(jobId: number) {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/cv/jobs/' + userId + '/' + jobId;

        return this.http.delete(url).pipe(
            map((data: CvJobModel) => {
                console.log('cv job deleted', data);
                return data;
            })
        );
    }

    public cvEducationCreate(type: string,
                             institution: string,
                             specialization: string,
                             city: string,
                             yearStart: number,
                             yearEnd: number) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/cv/education/' + userId + '/create';

        const formData = {
            'type':           type,
            'institution':    institution,
            'specialization': specialization,
            'city':           city,
            'year_start':     yearStart,
            'year_end':       yearEnd
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(url, formData, {headers: headers}).pipe(
            map((data: CvEducationModel) => {
                console.log('cv education create', data);
                return data;
            })
        );
    }

    public cvEducationGet() {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/cv/education/' + userId + '/education';

        return this.http.get(url).pipe(
            map((data: CvEducationModel[]) => {
                console.log('cv education get', data);
                return data;
            })
        );
    }

    public cvEducationDelete(educationId: number) {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/cv/education/' + userId + '/' + educationId;

        return this.http.delete(url).pipe(
            map((data: CvEducationModel) => {
                console.log('cv education deleted', data);
                return data;
            })
        );
    }

    public getUserCareerInfo() {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/career/info/' + userId + '/info';

        return this.http.get(url).pipe(
            map((data: CareerInfoModel) => {
                console.log('career info get', data);
                return data;
            })
        );
    }

    public updateUserCareerInfo(level: string,
                                category: string) {

        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/career/info/' + userId + '/update';

        const formData = {
            'level':    level,
            'category': category
        };

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(url, formData, {headers: headers}).pipe(
            map((data: CareerInfoModel) => {
                console.log('career info create/update', data);
                return data;
            })
        );
    }

    public getUserSalary() {
        const userId = this.userService.getUser() ? this.userService.getUser().public_id : null;
        const url = this.apiURL + '/users/career/info/' + userId + '/salary';

        return this.http.get(url).pipe(
            map((data: SalaryModel) => {
                console.log('salary get', data);
                return data;
            })
        );
    }

}