import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserModel } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private tokenCookieName: string = 'access_token';
  private userCookieName: string  = 'user';
  private expires: number         = 60 * 60 * 24; // 24 hours
  private accessToken: string     = null;
  private user: UserModel         = null;

  constructor(private http: HttpClient,
              private cookieService: CookieService) {}

  public login(response: any) {
    this.expires      = response.expires_in;
    this.accessToken  = response.access_token;
    this.user         = response.user;
    this.saveAccessToken(this.accessToken);
    this.saveUser(this.user);
  }

  public isAuthenticated() {
    return this.getAccessToken() !== null;
  }

  private saveAccessToken(token: string) {
    this.cookieService.set(this.tokenCookieName, token, this.expires, '/');
  }

  public getAccessToken() {
    if (this.accessToken === null) {
      let token = null;
      if (token = this.cookieService.get(this.tokenCookieName)) {
        this.accessToken = token;
      }
    }
    return this.accessToken;
  }

  private saveUser(user: UserModel) {
    this.cookieService.set(this.userCookieName, JSON.stringify(user), this.expires, '/');
  }

  public getUser() {
    if (this.user === null) {
      let jsonData = null;
      if (jsonData = this.cookieService.get(this.userCookieName)) {
        this.user = JSON.parse(jsonData);
      }
    }
    return this.user;
  }

  public updateUserName(firstName: string, lastName: string) {
    const user = this.getUser();

    if( (user.first_name !== firstName) || (user.last_name !== lastName) ) {
      this.user.first_name = firstName;
      this.user.last_name  = lastName;
      this.saveUser(this.user);
    }

  }

  public becomeProvider() {
    const user = this.getUser();
    this.user.is_provider = true;
    this.saveUser(this.user);
  }

  public changeThumbnail(url?: string) {
    const user = this.getUser();
    this.user.thumbnail = url;
    this.saveUser(this.user);
  }

  public logout() {
    this.accessToken  = null;
    this.user         = null;
    this.cookieService.delete(this.tokenCookieName);
    this.cookieService.delete(this.userCookieName);
  }

}
