import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// Models
import { KeyPersonModel } from '@shared/models/key-person.model';
import { CvJobModel } from '@shared/models/cv-job.model';
import { CvEducationModel } from '@shared/models/cv-education.model';

@Injectable({
  providedIn: 'root'
})
export class NotifyService {

    public newKeyPerson: EventEmitter<KeyPersonModel> = new EventEmitter();
    public newCvJob: EventEmitter<CvJobModel> = new EventEmitter();
    public newCvEducation: EventEmitter<CvEducationModel> = new EventEmitter();

}