import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Event, NavigationEnd } from '@angular/router';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {

  // Inputs
  private _search: string = '';
  private _category: string = '';
  @Input() set filters(value: any) {

    if(!isNullOrUndefined(value) && value.search) {
      this._search = value.search;
    }

    if(!isNullOrUndefined(value) && value.category) {
      this._category = value.category;
    }

  }

  // Search Form
  public searchForm: FormGroup;
  public search:     FormControl;
  public category:   FormControl;

  constructor(private router: Router) {}

  ngOnInit() {
    this.buildControls();
    this.buildForm();
  }

  private buildControls() {
    this.search   = new FormControl(this._search);
    this.category = new FormControl(this._category);
  }

  private buildForm() {
    this.searchForm = new FormGroup({
        search:   this.search,
        category: this.category
    });
  }

  public canSubmit() {
    return this.searchForm.valid;
  }

  public onSubmitForm() {

    var queryParams:any = {};

    if( !isNullOrUndefined(this.searchForm.value.search) && this.searchForm.value.search !== '' ) {
        queryParams.search = this.searchForm.value.search;
    }

    if( !isNullOrUndefined(this.searchForm.value.category) && this.searchForm.value.category !== '' ) {
        queryParams.category = this.searchForm.value.category;
    }

    this.router.navigate(['/jobs'], { queryParams: queryParams });

  }
  
}