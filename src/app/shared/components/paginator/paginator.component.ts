import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'app-paginator',
    templateUrl: './paginator.component.html',
    styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit {

    private _currentPage: number = 1;
    @Input() set currentPage(value: number) {
        this._currentPage = value;
        this.populatePagesArray();
    }
    get currentPage() {
        return this._currentPage;
    }

    private _lastPage: number = 1;
    @Input() set lastPage(value: number) {
        this._lastPage = value;
        this.populatePagesArray();
    }
    get lastPage() {
        return this._lastPage;
    }

    @Output() public changePage = new EventEmitter<number>();
    public pages: number[];

    constructor() {}

    public toPage(page: number) {
        if (page > 0 && page <= this.lastPage && page != this.currentPage) {
            this.changePage.emit(page);
        }
    }

    ngOnInit() {
        this.populatePagesArray();
    }

    public populatePagesArray() {
        this.pages = [];
        for(let i = this.currentPage-3 ; i < this.currentPage + 3; i++) {
            if( i < 1 || i > this.lastPage ) {
                continue;
            }
            this.pages.push(i);
        }
    }
}
