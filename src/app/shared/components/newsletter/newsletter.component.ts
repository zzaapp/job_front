import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';

// Components
import { ErrorModalComponent } from '@shared/components/modals/error/error.modal.component';
import { SuccessModalComponent } from '@shared/components/modals/success/success.modal.component';

@Component({
    selector: 'app-newsletter',
    templateUrl: './newsletter.component.html',
    styleUrls: ['./newsletter.component.css']
})
export class NewsletterComponent  implements OnInit {

    // Modals
    bsModalRef: BsModalRef;

    // Newsletter Form
    public newsletterForm: FormGroup;
    public email:          FormControl;

    // Alert Message
    public alert: AlertMessage = new AlertMessage();

    constructor(private httpService: HttpService,
                private modalService: BsModalService) {}

    ngOnInit() {
        this.buildControls();
        this.buildForm();
    }

    private buildControls() {
        this.email = new FormControl('', [Validators.required, Validators.email]);
    }

    private buildForm() {
        this.newsletterForm = new FormGroup({
            email:    this.email
        });
    }

    public canSubmit() {
        return this.newsletterForm.valid;
    }

    public onCloseAlert() {
        this.alert.reset();
    }

    public onSubmitForm() {
        this.alert.reset();

        this.httpService.subscribeToNewsletter(this.newsletterForm.value.email).subscribe(
        (data: any) => {
            this.alert = new AlertMessage(true, AlertMessage.TYPE_SUCCESS, 'You successfully subscribed to our newsletter.');
            this.showAlertModal(this.alert);
        },
        (error: HttpErrorResponse) => {
            const status    = error.status;    
            switch (status) {
            case 400:
                const errorKey = HelperService.getObjectFirstKey(error.error);
                this.alert     = new AlertMessage(true, AlertMessage.TYPE_ERROR, errorKey ? errorKey[0] : 'Newsletter error.');
                break;
            case 403:
            case 404:
            case 500:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, error.error.error);
                break;
            default:
                this.alert = new AlertMessage(true, AlertMessage.TYPE_ERROR, 'An error occured.');
                break;
            }
            this.showAlertModal(this.alert);
        }
        );

    }

    private showAlertModal(alert: AlertMessage) {
        const initialState = {
            alert: alert
        };

        switch (alert.type) {
        case AlertMessage.TYPE_ERROR:
            this.bsModalRef = this.modalService.show(ErrorModalComponent, {initialState});
            break;
        case AlertMessage.TYPE_SUCCESS:
            this.bsModalRef = this.modalService.show(SuccessModalComponent, {initialState});
            break;
        }

        this.resetForm();

    }

    private resetForm() {
        this.newsletterForm.reset();
    }

}
