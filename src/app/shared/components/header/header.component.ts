import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

// Services
import { HttpService } from '@shared/services/http.service';
import { HelperService } from '@shared/services/helper.service';
import { UserService } from '@shared/services/user.service';

// Models
import { UserModel } from '@shared/models/user.model';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    constructor(private httpService: HttpService,
                private router: Router,
                private userService: UserService) {}

    ngOnInit() {}

    public logout() {

        this.httpService.logoutUser(this.userService.getUser().public_id).subscribe(
          (data: any) => {
            this.userService.logout();
            this.router.navigate(['login']);
          },
          (error: HttpErrorResponse) => {
            this.userService.logout();
            this.router.navigate(['login']);
          }
        );

    }

}
