import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';

@Component({
    selector: 'app-modal-error',
    templateUrl: './error.modal.component.html',
    styleUrls: ['./error.modal.component.css']
})
export class ErrorModalComponent {
    
    public alert: AlertMessage;

    constructor(public bsModalRef: BsModalRef) {}

}
