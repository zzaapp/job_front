import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

// Models
import { AlertMessage } from '@shared/models/alert.message.model';

@Component({
    selector: 'app-modal-success',
    templateUrl: './success.modal.component.html',
    styleUrls: ['./success.modal.component.css']
})
export class SuccessModalComponent {
    
    public alert: AlertMessage;

    constructor(public bsModalRef: BsModalRef) {}

}
