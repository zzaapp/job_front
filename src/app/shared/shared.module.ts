import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CookieService } from 'ngx-cookie-service';

// Components
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { NewsletterComponent } from './components/newsletter/newsletter.component';
import { ErrorModalComponent } from './components/modals/error/error.modal.component';
import { SuccessModalComponent } from './components/modals/success/success.modal.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { RegisterSectionComponent } from './components/register-section/register-section.component';
import { SearchFormComponent } from './components/search-form/search-form.component';

// Interceptors
import { UserInterceptor } from './interceptors/user.interceptor';

@NgModule({
  declarations: [
      HeaderComponent,
      FooterComponent,
      NewsletterComponent,
      ErrorModalComponent,
      SuccessModalComponent,
      BreadcrumbComponent,
      PaginatorComponent,
      RegisterSectionComponent,
      SearchFormComponent
  ],
  imports: [
      RouterModule,
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      BsDropdownModule.forRoot(),
      ModalModule.forRoot(),
      CarouselModule.forRoot()
  ],
  exports: [
      RouterModule,
      FormsModule, 
      ReactiveFormsModule,
      BsDropdownModule,
      ModalModule,
      CarouselModule,
      HeaderComponent,
      FooterComponent,
      NewsletterComponent,
      ErrorModalComponent,
      SuccessModalComponent,
      BreadcrumbComponent,
      PaginatorComponent,
      RegisterSectionComponent,
      SearchFormComponent
  ],
  providers: [
      CookieService,
      {
        provide: HTTP_INTERCEPTORS,
        useClass: UserInterceptor,
        multi: true
      }
  ],
  entryComponents: [
    ErrorModalComponent,
    SuccessModalComponent
  ]
})
export class SharedModule { }
