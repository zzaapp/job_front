export interface ISocialModel {
    id?: number;
    facebook?: string;
    instagram?: string;
    twitter?: string;
    dribbble?: string;
}

export class SocialModel implements ISocialModel {
    public id        = null;
    public facebook  = null;
    public instagram = null;
    public twitter   = null;
    public dribbble  = null;

    constructor(model?: ISocialModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}