export interface ICvJobModel {
    id?: number;
    position?: string;
    company?: string;
    year_start?: number;
    year_end?: number;
    responsibilities?: string;
}

export class CvJobModel implements ICvJobModel {
    public id               = null;
    public position         = null;
    public company          = null;
    public year_start       = null;
    public year_end         = null;
    public responsibilities = null;

    constructor(model?: ICvJobModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}