export interface ISalaryModel {
    id?: number;
    level?: string;
    category?: string;
    salary?: number;
}

export class SalaryModel implements ISalaryModel {
    public id       = null;
    public level    = null;
    public category = null;
    public salary   = null;

    constructor(model?: ISalaryModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}