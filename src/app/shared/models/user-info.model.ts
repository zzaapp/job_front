export interface IUserInfoModel {
    first_name?: string;
    last_name?: string;
    country?: string;
    county?: string;
    address?: string;
    spoken_languages?: string;
    hobbies?: string;
    curiosities?: string;
    thumbnail?: string;
}

export class UserInfoModel implements IUserInfoModel {
    public first_name       = null;
    public last_name        = null;
    public country          = null;
    public county           = null;
    public address          = null;
    public spoken_languages = null;
    public hobbies          = null;
    public curiosities      = null;
    public thumbnail        = null;

    constructor(model?: IUserInfoModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}