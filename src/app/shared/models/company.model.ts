export interface ICompanyModel {
    id?: number;
    company_description?: string;
    company_name?: string;
    company_evolution?: string;
    company_current?: string;
}

export class CompanyModel implements ICompanyModel {
    public id                  = null;
    public company_description = null;
    public company_name        = null;
    public company_evolution   = null;
    public company_current     = null;

    constructor(model?: ICompanyModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}