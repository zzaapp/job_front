export interface ICareerInfoModel {
    id?: number;
    level?: string;
    category?: string;
}

export class CareerInfoModel implements ICareerInfoModel {
    public id       = null;
    public level    = null;
    public category = null;

    constructor(model?: ICareerInfoModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}