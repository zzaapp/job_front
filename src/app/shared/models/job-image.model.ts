export interface IJobImageModel {
    id?: number;
    path?: number;
}

export class JobImageModel implements IJobImageModel {
    public id          = null;
    public path      = null;

    constructor(model?: IJobImageModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}