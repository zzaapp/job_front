export interface IKeyPersonModel {
    id?: number;
    name?: string;
    position?: string;
    path?: string;
}

export class KeyPersonModel implements IKeyPersonModel {
    public id       = null;
    public name     = null;
    public position = null;
    public path     = null;

    constructor(model?: IKeyPersonModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}