export interface IPaginatorRequestModel {
    page?: number;
    filter?: any;
}

export class PaginatorRequestModel implements IPaginatorRequestModel {
    public page: number;
    public filter: any;

    constructor(model?: IPaginatorRequestModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}
