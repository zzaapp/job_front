import {isNullOrUndefined} from 'util';

export interface IApplicationModel {
    id?: number;
    job_description?: string;
    job_id?: number;
    job_title?: string;
    status?: string;
}

export class ApplicationModel implements IApplicationModel {
    public id               = null;
    public job_description  = null;
    public job_id           = null;
    public job_title        = null;
    public status           = null;

    constructor(model?: IApplicationModel) {
        if (model) {
            Object.assign(this, model);
        }
    }

    public descriptionPreview() {
        if(isNullOrUndefined(this.job_description)) {
            return '';
        }
        return this.job_description.substring(0,40) + '...';
    }
}