export interface ICvEducationModel {
    id?: number;
    type?: string;
    institution?: string;
    specialization?: string;
    city?: string;
    year_start?: number;
    year_end?: number;
}

export class CvEducationModel implements ICvEducationModel {
    public id             = null;
    public type           = null;
    public institution    = null;
    public specialization = null;
    public city           = null;
    public year_start     = null;
    public year_end       = null;

    constructor(model?: ICvEducationModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}