export interface IJobModel {
    id?: number;
    active?: number;
    title?: string;
    category?: string;
    description?: string;
    duration?: string;
    included?: string;
    thumbnail?: string;
    company?: string;
}

export class JobModel implements IJobModel {
    public id          = null;
    public active      = null;
    public title       = null;
    public category    = null;
    public description = null;
    public duration    = null;
    public included    = null;
    public thumbnail   = null;
    public company     = null;

    constructor(model?: IJobModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}