import { UserModel } from '@shared/models/user.model';

export interface IReviewModel {
    id?: number;
    comment?: string;
    rating?: number;
    reviewer?: UserModel;
}

export class ReviewModel implements IReviewModel {
    public id       = null;
    public comment  = null;
    public rating   = null;
    public reviewer = null;

    constructor(model?: IReviewModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}