import {isNullOrUndefined} from 'util';

import { UserModel } from '@shared/models/user.model';

export interface IAppointmentModel {
    id?: number;
    job_description?: string;
    job_id?: number;
    job_title?: string;
    status?: string;
    user?: UserModel;
}

export class AppointmentModel implements IAppointmentModel {
    public id               = null;
    public job_description  = null;
    public job_id           = null;
    public job_title        = null;
    public status           = null;
    public user             = null;

    constructor(model?: IAppointmentModel) {
        if (model) {
            Object.assign(this, model);
        }
    }

    public descriptionPreview() {
        if(isNullOrUndefined(this.job_description)) {
            return '';
        }
        return this.job_description.substring(0,40) + '...';
    }
}