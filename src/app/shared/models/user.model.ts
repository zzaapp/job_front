export interface IUserModel {
    public_id?: string;
    first_name?: string;
    last_name?: string;
    email?: string;
    active?: number;
    is_provider?: boolean;
    thumbnail?: string;
}

export class UserModel implements IUserModel {
    public public_id;
    public first_name;
    public last_name;
    public email;
    public active;
    public is_provider;
    public thumbnail;

    constructor(model?: IUserModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}