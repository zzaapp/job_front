export interface IProviderModel {
    public_id?: string;
    first_name?: string;
    last_name?: string;
    email?: string;
    active?: boolean;
    thumbnail?: string;
}

export class ProviderModel implements IProviderModel {
    public public_id   = null;
    public first_name  = null;
    public last_name   = null;
    public email       = null;
    public active      = null;
    public thumbnail   = null;

    constructor(model?: IProviderModel) {
        if (model) {
            Object.assign(this, model);
        }
    }
}