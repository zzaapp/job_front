export class AlertMessage {
    public static TYPE_NONE: number    = 0;
    public static TYPE_ERROR: number   = 1;
    public static TYPE_WARNING: number = 2;
    public static TYPE_SUCCESS: number = 3;

    public show: boolean = false;
    public type: number;
    public message: string;

    public constructor(show: boolean = false, type: number = AlertMessage.TYPE_NONE, message: string = '') {
        this.show = show;
        this.type = type;
        this.message = message;
    }

    public reset() {
        this.show   = false;
        this.type   = AlertMessage.TYPE_NONE;
        this.message= '';
    }
}