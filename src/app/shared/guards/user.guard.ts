import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { UserService } from "@shared/services/user.service";

@Injectable()
export class UserGuard implements CanActivate {

    public constructor(private userService: UserService,
                       private router: Router) {}

    public canActivate(): boolean {
        if (this.userService.isAuthenticated()) {
            return true;
        }
        this.router.navigate(['login']);
        return false;
    }
}