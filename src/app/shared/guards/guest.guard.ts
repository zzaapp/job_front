import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { UserService } from "@shared/services/user.service";

@Injectable()
export class GuestGuard implements CanActivate {

    public constructor(private userService: UserService,
                       private router: Router) {}

    public canActivate(): boolean {
        if (this.userService.isAuthenticated()) {
            this.router.navigate(['user/dashboard']);
            return false;
        }
        return true;
    }
}