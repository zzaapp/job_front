import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UserModule } from "./user/user.module";
import { AppRoutingModule } from "./app-routing.module";
import { GuestModule } from "./guest/guest.module";
import { VisitorModule } from "./visitor/visitor.module";
import { SharedModule } from "@shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
      BrowserModule,
      ReactiveFormsModule,
      SharedModule,
      GuestModule,
      VisitorModule,
      UserModule,
      AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
