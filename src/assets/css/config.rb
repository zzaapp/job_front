# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "../"
css_dir = "/"
sass_dir = "sass"
images_dir = "../img"
javascripts_dir = "../js"


# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed

output_style = :expanded

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false

preferred_syntax = :sass

# Fixes the limit on the amount of selectors on IE9
# and runs a second compilation for production files
on_stylesheet_saved do |filename|

    system('blessc ' + File.basename(filename) + ' --force')
  	`compass compile -c config_prod.rb --force`
  	
end

cache_dir = "../../../.sass-cache/.ui-library"